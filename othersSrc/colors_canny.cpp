
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// Global variables




int main(int argc, char ** argv )
{

	Mat input = imread( argv[1] );
	cv::Mat hsv;
	cv::cvtColor(input,hsv,CV_BGR2HSV);

	std::vector<cv::Mat> channels;
	cv::split(hsv, channels);

	cv::Mat H = channels[0];
	cv::Mat S = channels[1];
	cv::Mat V = channels[2];

	cv::Mat shiftedH = H.clone();
	int shift = 25; // in openCV hue values go from 0 to 180 (so have to be doubled to get to 0 .. 360) because of byte range from 0 to 255
	for(int j=0; j<shiftedH.rows; ++j)
	    for(int i=0; i<shiftedH.cols; ++i)
	    {
	        shiftedH.at<unsigned char>(j,i) = (shiftedH.at<unsigned char>(j,i) + shift)%180;
	    }

	cv::Mat cannyH;
	cv::Canny(shiftedH, cannyH, 100, 50);

	cv::Mat cannyS;
	cv::Canny(S, cannyS, 200, 100);	
	// extract contours of the canny image:
	std::vector<std::vector<cv::Point> > contoursH;
	std::vector<cv::Vec4i> hierarchyH;
	cv::findContours(cannyH,contoursH, hierarchyH, CV_RETR_TREE , CV_CHAIN_APPROX_SIMPLE);

	// draw the contours to a copy of the input image:
	cv::Mat outputH = input.clone();
	for( int i = 0; i< contoursH.size(); i++ )
	{
		cv::drawContours( outputH, contoursH, i, cv::Scalar(0,0,255), 2, 8, hierarchyH, 0);
	}
	cv::dilate(cannyH, cannyH, cv::Mat());
	cv::dilate(cannyH, cannyH, cv::Mat());
	cv::dilate(cannyH, cannyH, cv::Mat());

	// Dilation before contour extraction will "close" the gaps between different 	objects b
	namedWindow("results0",WINDOW_NORMAL);
	resizeWindow("results0" , 600,600);
	imshow("results0", outputH );
	

	outputH = input.clone();
	for( int i = 0; i< contoursH.size(); i++ )
	{
		if(cv::contourArea(contoursH[i]) < 20) continue; // ignore contours that are too small to be a patty
		if(hierarchyH[i][3] < 0) continue;  // ignore "outer" contours

		cv::drawContours( outputH, contoursH, i, cv::Scalar(0,0,255), 2, 8, hierarchyH, 0);
	}

	namedWindow("results",WINDOW_NORMAL);
	resizeWindow("results" , 600,600);
	imshow("results", outputH );
	waitKey(0);

}