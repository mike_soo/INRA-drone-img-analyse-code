#include "ViewManager.hpp"

ViewManager::~ViewManager()
{
	
}

void ViewManager::displayImage()
{
	{	
			{
				cout<<"ViewManager::displayImage() : try lock"<<endl;	
				tbb::interface5::unique_lock<tbb::mutex> ulDisplay (this->displayViewManagerMutex);
				cout<<"ViewManager::displayImage() : got lock"<<endl;	
				if(!displayerInUse)
				{
					cout<<"ViewManager::displayImage()," <<"tid="<<thread::id()<<" : waiting for image to show ..."<<endl;

					displayerInUse = true;
					display.wait(ulDisplay);

					cout<<"ViewManager::displayImage() : waiting for image to show ok"<<endl;


				}
				else
				{
					cout<<"ViewManager::displayImage(): waiting for displayer ..."<<endl;
					

					display.wait(ulDisplay);
					

					cout<<"ViewManager::displayImage(): waiting for displayer ok"<<endl;

					displayerInUse = true;
				}
				cout<<"ViewManager::displayImage() : end lock"<<endl;	
			}
	}

}

void ViewManager::displayImageFromMainThread()
{
	this->createWindow();
	this->showImage();
	this->setControllers();
	this->cleanHeader();
}

void ViewManager::waitForImageToDisplay()
{
	while(1)
	{
		// {
		// 	cout<<"ViewManager::waitForImageToDisplay() "<<"tid="<<thread::id()<<" : try lock"<<endl;	
		// 	tbb::interface5::unique_lock<tbb::mutex> ulDisplay (this->displayViewManagerMutex);
		// 	cout<<"ViewManager::waitForImageToDisplay() : got lock "<<endl;	
		// 	if(displayerInUse)
		// 	{
		// 		cout<<"ViewManager::waitForImageToDisplay() : Displayer in use"<<endl;
		// 		this->createWindow();
		// 		cout<<"ViewManager::waitForImageToDisplay() : WindowCreated"<<endl;
		// 		this->showImage();
		// 		cout<<"ViewManager::waitForImageToDisplay() : ImageShowed"<<endl;
		// 		this->setControllers();

		// 		this->cleanHeader();
		// 		displayerInUse = false;
				
		// 		display.notify_one();

				
		// 	}
		// 	cout<<"ViewManager::waitForImageToDisplay() : end lock "<<endl;	
		// }
		this->wait();
		
	}
}


void ViewManager::lock()
{
	this->settingHeader.lock();
}
void ViewManager::unlock()
{
	this->settingHeader.unlock();
}


void  ViewManager::setWinName(string &winName)
{
	this->winName = winName;
}
void  ViewManager::setWinWidth(int winWidth)
{	
	this->winWidth = winWidth;
}
void  ViewManager::setWinHeight(int winHeight)
{
	this->winHeight = winHeight;
}
void  ViewManager::setImageToDisplay(shared_ptr<Image> imageToDisplay) 
{
	this->imageToDisplay = imageToDisplay;
}

const string ViewManager::getWinName()
{
	return this->winName;
}
const int  ViewManager::getWinWidth()
{	
	return this->winWidth;
}
const int  ViewManager::getWinHeight()
{
	return this->winHeight;
}
const shared_ptr<Image>  ViewManager::getImageToDisplay()
{
	return this->imageToDisplay;
}



void ViewManager::askForViewManager()
{
	this->lock();
}

void ViewManager::releaseViewManager()
{
	this->unlock();
}