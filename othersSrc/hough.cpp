#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <map>
using namespace cv;
using namespace std;

void help()
{
 cout << "\nThis program demonstrates line finding with the Hough transform.\n"
         "Usage:\n"
         "./houghlines <image_name>, Default is pic1.jpg\n" << endl;
}

int main(int argc, char** argv)
{
 const char* filename = argc >= 2 ? argv[1] : "pic1.jpg";

 Mat src = imread(filename, 0);
 if(src.empty())
 {
     help();
     cout << "can not open " << filename << endl;
     return -1;
 }

 Mat dst, cdst;

 cout<<"main: "<<src.type()<<endl;
 Canny(src, dst, 100, 200, 3);
 cvtColor(dst, cdst, CV_GRAY2BGR);

 #if 0
  vector<Vec2f> lines;
  HoughLines(dst, lines, 1, CV_PI/180, 100, 0, 0 );

  for( size_t i = 0; i < lines.size(); i++ )
  {
     float rho = lines[i][0], theta = lines[i][1];
     Point pt1, pt2;
     double a = cos(theta), b = sin(theta);
     double x0 = a*rho, y0 = b*rho;
     pt1.x = cvRound(x0 + 1000*(-b));
     pt1.y = cvRound(y0 + 1000*(a));
     pt2.x = cvRound(x0 - 1000*(-b));
     pt2.y = cvRound(y0 - 1000*(a));
     line( cdst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
  }
 #else
  vector<Vec4i> lines;
  vector<double> angles;
  const int resolutionDenominator = 180;

  HoughLinesP(dst, lines, 1, (double)CV_PI/resolutionDenominator, 50, 10, 1 );
  for( size_t i = 0; i < lines.size(); i++ )
  {
    Vec4i l = lines[i];
    line( cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, CV_AA);
  }

  double theta;
  for(unsigned int i = 0 ; i < lines.size() ; i++)
  {

    Point2i vp1 (lines.at(i)(2) - lines.at(i)(0) , lines.at(i)(3) - lines.at(i)(1) );
    theta = atan2( vp1.y , vp1.x  ) * 180.0 / CV_PI;
    theta = static_cast<double>(static_cast<int>(theta * 10) / 10.0);
    
    if(theta<0)
      theta = 360.0 + theta ;
    angles.push_back(theta);
    // cout<<"theta"<<i<<"="<<theta<<endl;
  }

  map <unsigned int , unsigned int> anglesHisto;
  map<unsigned int , unsigned int>::iterator anglesHistoIt ;
  unsigned anglesInts[360]={};

  unsigned int angleIndex = 0;
  for(unsigned int i=0 ; i < angles.size() ; i++)
  {
    // calculating index of angle found in angles 
    angleIndex = angles.at(i)*10;
    // looking if index exist already
    anglesHistoIt=anglesHisto.find(angleIndex);

    // If it does not exits, create it and set its cumulative associated value to 1.
    if(anglesHistoIt == anglesHisto.end())
      anglesHisto.insert(pair<unsigned int , unsigned int> (angleIndex , 1));
    // If it does exists, set its cumulative value to : old cumulative value + 1
    else
      anglesHisto.insert(pair<unsigned int , unsigned int> (angleIndex , anglesHistoIt->second + 1));
  }

  // We loop through all values of the histogram of angles values to create 
  // a second histogram differentiated by its integers indexes.
  for(anglesHistoIt = anglesHisto.begin() ; anglesHistoIt != anglesHisto.end() ; ++anglesHistoIt)
  {
    cout<<anglesHistoIt->first / 10.0<<" : "<<anglesHistoIt->second<<endl;
    anglesInts[static_cast<int>(anglesHistoIt->first /10.0)]++;
  }

  for(unsigned int i = 0 ; i < 360 ; i++)
  {
    cout<<i<<" : "<<anglesInts[i]<<endl;
  }

  unsigned int anglesMaxCumulative= anglesInts[0];
  unsigned int anglesMaxCumulativeIndex = 0;
  for(unsigned int i= 1 ; i < 360 ; i++)
  {
    if(anglesInts[i]> anglesMaxCumulative)
    {
      anglesMaxCumulative = anglesInts[i];
      anglesMaxCumulativeIndex = i;
    }
  }

  cout<<"anglesMaxCumulative="<<anglesMaxCumulative<<endl;
  cout<<"anglesMaxCumulativeIndex="<<anglesMaxCumulativeIndex<<endl;
  unsigned int nbOfAnglesMax= 0;
  unsigned int sumOfAngles=0;
  anglesMaxCumulativeIndex *= 10;
  for(unsigned int i = anglesMaxCumulativeIndex ; i < anglesMaxCumulativeIndex + 11 ; i ++)
  {
    anglesHistoIt= anglesHisto.find(i);
    if(anglesHistoIt != anglesHisto.end())
    // If it does exists, set its cumulative value to : old cumulative value + 1
    {
     
      nbOfAnglesMax += anglesHistoIt->second;
      sumOfAngles += anglesHistoIt->first * anglesHistoIt->second;
    }
  }
  double angleOfRotation = static_cast<double> (sumOfAngles / nbOfAnglesMax / 10.0);


  cout<<"sumOfAngles="<<sumOfAngles<<"  nbOfAnglesMax="<<nbOfAnglesMax<<"  angleOfRotation="<< angleOfRotation<<endl;


  

  Mat dstRot;

  Point2f center(src.cols/2.0, src.rows/2.0); 
  Mat rot = getRotationMatrix2D(center, angleOfRotation, 1.0);
  Rect bbox = RotatedRect(Point2f(),src.size(), angleOfRotation).boundingRect2f();

  rot.at<double>(0,2) += bbox.width/2.0 - center.x;
  rot.at<double>(1,2) += bbox.height/2.0 - center.y;

  warpAffine(src, dstRot, rot, bbox.size());
  
  
  
      
 #endif
  namedWindow("source",WINDOW_NORMAL);
  resizeWindow("source",600,600);
  imshow("source", src);
 
 namedWindow("detected lines",WINDOW_NORMAL);
 resizeWindow("detected lines",600,600);
 imshow("detected lines", dstRot);

 waitKey();

 return 0;
}