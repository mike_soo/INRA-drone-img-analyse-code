#ifndef LAPLACIANFILTER_HPP
#define LAPLACIANFILTER_HPP
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "Filter.hpp"
#include "Image.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for the Laplacian filter containing essential attributes and
 *             methods for the filtering procedure.
 */
class LaplacianFilter : public Filter
{

private:
public:
	virtual void applyFilter(Image* image);
	virtual void applyFilter(shared_ptr<Image> image);
};

#endif 