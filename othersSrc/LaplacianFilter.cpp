#include "LaplacianFilter.hpp"


/**
 * @brief      Override function allows to apply gaussian filtering to an
 *             Image instance.
 *
 * @details    Called by Filter::applyFilter, will modify the image attribute of
 *             the Filter class.
 *
 * @param[in]  image  The image
 */
void LaplacianFilter::applyFilter(shared_ptr<Image> image)
{
	int kernel_size = 3;
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;
	Mat imageGrey;
	cvtColor (image->getImageMat() , imageGrey , COLOR_RGB2GRAY );
	namedWindow("Grey Image",WINDOW_NORMAL);
	resizeWindow("Grey Image" , 600 , 600);
	imshow("Grey Image",imageGrey);
	waitKey(1);
	Laplacian(imageGrey , image->getImageMat() , ddepth , kernel_size , scale , delta , BORDER_DEFAULT );
	convertScaleAbs(image->getImageMat() , image->getImageMat() );	
}