#include "OpencvViewManager.hpp"
  
OpencvViewManager::OpencvViewManager() 
{

}

void OpencvViewManager::createWindow()
{
	
	if(!cvGetWindowHandle(this->getWinName().c_str()))
	{
		namedWindow(this->getWinName() , this->getFlags());
		resizeWindow(this->getWinName() , this->getWinWidth() , this->getWinHeight() );
	}
}
void OpencvViewManager::showImage()
{
	
	imshow(this->getWinName() , this->getImageToDisplay()->getMat());
	
}

 
int OpencvViewManager::getFlags()
{
	return this->flags;
}

void OpencvViewManager::wait()
{
	waitKey(20);
}

void OpencvViewManager::setMouseListener()
{
	if(this->onMouse != nullptr)
		setMouseCallback(this->getWinName(), this->onMouse , this->mouseCallbackParams);
}

void OpencvViewManager::setOnMouse(MouseCallback onMouse , void * mouseCallbackParams)
{
	this->onMouse = onMouse;
	this->mouseCallbackParams = mouseCallbackParams;
}


void OpencvViewManager::setTrackBars(vector <string> trackbarNames,vector<int*>values , vector<int>counts , vector<TrackbarCallback> trackbarCallbacks,vector<void*> userdatas)
{
	for(unsigned int i = 0 ; i < values.size() ; i ++)
	{
		createTrackbar(trackbarNames.at(i) , this->getWinName() , values.at(i) , counts.at(i) , trackbarCallbacks.at(i) , userdatas.at(i)  );
	}
}

void OpencvViewManager::setTrackBar(string trackbarName , int* trackbarValue  , int trackbarCount , TrackbarCallback trackbarCallback , void* userdata)
{
	this->trackbarName = trackbarName ; 
	this->trackbarValue = trackbarValue ;

	this->trackbarCount = trackbarCount;
	this->trackbarCallback = trackbarCallback;
	this->userdata = userdata;	
}


void OpencvViewManager::cleanHeader()
{
	this->onMouse = nullptr;
	this->mouseCallbackParams = nullptr;
	this->trackbarName = "";
	this->trackbarValue = nullptr;
	this->trackbarCount = 0;
	this->trackbarCallback= nullptr;
}



void OpencvViewManager::setControllers()
{
	
	if(this->trackbarName != "" and this->trackbarCallback != nullptr)
		createTrackbar(this->trackbarName , this->getWinName() , this->trackbarValue , this->trackbarCount , this->trackbarCallback , this->userdata );

	if(this->onMouse != nullptr)
		setMouseCallback(this->getWinName() , this->onMouse , this->mouseCallbackParams );
}