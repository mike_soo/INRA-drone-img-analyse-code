#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using namespace cv;
using namespace std;
/// Global variables

int threshold_value = 0;
int threshold_type = 3;;
int threshold_save = 0;
int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;
string fileName = "";

Mat src, src_gray, dst;
const char* window_name = "Threshold Demo";

const char* trackbar_type = "Type: \n 0: Binary \n 1: Binary Inverted \n 2: Truncate \n 3: To Zero \n 4: To Zero Inverted";
const char* trackbar_value = "Value";

const char* trackbar_save = "Save";

/// Function headers
void Threshold_Demo( int, void* );
void threshold_save_img(int , void* );
/**
 * @function main
 */


int main( int argc, char** argv )
{
  /// Load an image
  src = imread( argv[1], 1 );
  fileName = argv[1];

  /// Convert the image to Gray
  cvtColor( src, src_gray, COLOR_RGB2GRAY );

  /// Create a window to display results
  namedWindow( window_name, WINDOW_NORMAL );
  resizeWindow(window_name, 600,600);
  /// Create Trackbar to choose type of Threshold
  createTrackbar( trackbar_type,
                  window_name, &threshold_type,
                  max_type, Threshold_Demo );

  createTrackbar( trackbar_value,
                  window_name, &threshold_value,
                  max_value, Threshold_Demo );

  createTrackbar( trackbar_save,
                  window_name, &threshold_save,
                  1 , threshold_save_img);
  /// Call the function to initialize
  Threshold_Demo( 0, 0 );

  /// Wait until user finishes program
  waitKey(0);

}



void threshold_save_img(int , void* params)
{
  string segment;
  vector<std::string> seglist;
  std::stringstream ssfileName(fileName);
  while(std::getline(ssfileName, segment, '/'))
  {
     seglist.push_back(segment);
  }
 

  string sfileName = seglist.front() + "/";

  sfileName += "thres_";
  sfileName +=  to_string(threshold_value);
  sfileName += string("_")+ seglist.back();
  

  cout<<"file to write :"<<sfileName<<endl;
  imwrite( sfileName  , dst );
}

/**
 * @function Threshold_Demo
 */
void Threshold_Demo( int, void* )
{
  /* 0: Binary
     1: Binary Inverted
     2: Threshold Truncated
     3: Threshold to Zero
     4: Threshold to Zero Inverted
   */


  threshold( src_gray, dst, threshold_value, max_BINARY_value,threshold_type );

  imshow( window_name, dst );
}