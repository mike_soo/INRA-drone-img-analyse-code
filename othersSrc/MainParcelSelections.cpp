#include "MainParcelSelections.hpp"
#include "ImageInfoExtractor.hpp"   


bool selectObject = false;
Point origin;
Rect selection;
bool mainParcelSelected = false;


/**
    Callback function charged of the main parcel selection.

    @param event Event type
    @param x Horizontal coordinate of the click event.
    @param y Vertical coordinate of the click event.
    @param  
    @param 
*/
MainParcelSelections::MainParcelSelections(shared_ptr <ImageInfoExtractor> imageInfoExtractor) : State(imageInfoExtractor)
{
    string stateName="MainParcelSelections";
    this->setStateName(stateName);
}

MainParcelSelections::~MainParcelSelections()
{
    cout<<this->getStateName()<<"::~MainParcelSelections()"<<endl;
}

void MainParcelSelections::init(){}

static void onMouse( int event, int x, int y, int, void* mainParcelSelectionsPtr)
{
    // TODO : The instance of mainParcelSelections is actually pointed by smart pointers while it's passed to this function with a raw pointer. A way of passing a smart pointer should be implemented.
    
	MainParcelSelections *mainParcelSelections = (MainParcelSelections*) mainParcelSelectionsPtr ;

	if(!mainParcelSelections)
		return;
    


    if( selectObject )
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);
        
        selection &= Rect(0, 0, mainParcelSelections->getMainImage()->getMat().cols, mainParcelSelections->getMainImage()->getMat().rows);

        

        if( selection.width > 0 && selection.height > 0 )
        {
            
            Mat temp = mainParcelSelections->getMainImage()->getMat().clone();
            Mat roi(temp , selection);
            bitwise_not(roi,roi);
            string winName ="Set orientation of selection";
            // mainParcelSelections->getViewManager()->askForViewManager();
            mainParcelSelections->getViewManager()->setWinName(winName);
            namedWindow(winName , WINDOW_NORMAL);
            resizeWindow(winName , 600 , 600);
            imshow(winName , temp);
        }


        

    }
    
    switch( event )
    {
    case EVENT_LBUTTONDOWN:
        {
            origin = Point(x,y);
            selection = Rect(x,y,0,0);
            selectObject = true;
            
        }
        break;
    case EVENT_LBUTTONUP:
        selectObject = false;
        if( selection.width > 0 && selection.height > 0 )
        {
            mainParcelSelections->addSelection();

            /*Showing last modification of the recent created selection.*/
            Mat roi(mainParcelSelections->getMainImage()->getMat() , selection);
            bitwise_not(roi,roi);
            // imshow("Main Parcel Selections",mainParcelSelections->getMainImage()->getMat());
            
            /*Creating and showing cropped image from selection. */
            

            /*Analysis of cropped image. */
            unsigned int idSelectedParcel =mainParcelSelections->nbrOfSelections() - 1 ;
            // Creation of selected image .
            Mat croppedImage = mainParcelSelections->getMainImage()->getMat()(selection).clone();
            
            // Creation of SelectedParcelAnalysis state.
            unique_ptr<SelectedParcelAnalysis> selectedParcelAnalysis = make_unique<SelectedParcelAnalysis>(mainParcelSelections->getImageInfoExtractor() , idSelectedParcel , make_unique<Image>(croppedImage));
            
            unsigned int selectedParcelAnalysisId = State::registerState(move(selectedParcelAnalysis));
            State::execState(selectedParcelAnalysisId);


            // tbb_thread th1([selectedParcelAnalysis]()
            // {    
            //     // Execution of analysis (SelectedParcelAnalysis state execution).
            //     selectedParcelAnalysis->exec();
            // });
            
            
        }
        else if(selection.width == 0 && selection.height== 0)
        {
            // Case if button only has been pressed with out creating any selection
            // In this case we asume that an attempt to rotate the image is process.
            mainParcelSelections->addPointForRotation(origin);
        }
        break;

    // case EVENT_MOUSEHWHEEL:
        // cout<<"MainParcelSelections onMouse() : wheel detected"<<endl;
    }



    
}

void MainParcelSelections::addSelection()
{
	shared_ptr <Rect> rect_ptr = make_shared<Rect>(selection);
	this->mainParcelSelections.push_back(rect_ptr);
}

shared_ptr<Rect> MainParcelSelections::getSelection(unsigned int index)
{
	if(index >= 0 && index < this->mainParcelSelections.size())
		return this->mainParcelSelections.at(index);
	
	return  unique_ptr<Rect>();
}


void buttonCallback(int state,void*)
{
    cout<<"MainParcelSelections buttonCallback() : button pressed"<<endl;
}

void rotating(int, void* mainParcelSelectionsPtr)
{
    MainParcelSelections* mainParcelSelections = static_cast<MainParcelSelections*>(mainParcelSelectionsPtr);

    mainParcelSelections->rotateImage(mainParcelSelections->angleOfImageWithSelections);
}

void MainParcelSelections::execBehavior()
{
    cout<<"MainParcelSelections execBehavior() : setting image with selections"<<endl; 
    this->setImageWithSelections();
    cout<<"MainParcelSelections execBehavior() : setting points for rotation"<<endl;
    this->setPointsForRotation();
    cout<<"MainParcelSelections execBehavior() : start"<<endl;      
    
    
    
     
    
    string winName = "Main Parcel Selections";
    TYPE_OF_VIEW_MANAGER * viewManager = static_cast<TYPE_OF_VIEW_MANAGER*>(this->getViewManager());
    
    
    viewManager->setWinName(winName);
    viewManager->setImageToDisplay(this->getMainImage());
    viewManager->setOnMouse(onMouse , (void*) this);    
    

    viewManager->displayImageFromMainThread();



    /*UNITARY TEST , for debuging purpose only*/
        selection = Rect(0 , 0 ,  this->getMainImage()->getMat().cols , this->getMainImage()->getMat().rows);

        this->addSelection();

        /*Showing last modification of the recent created selection.*/
        Mat roi(this->getMainImage()->getMat() , selection);
        bitwise_not(roi,roi);
        // imshow("Main Parcel Selections",this->getMainImage()->getMat());
        
        /*Creating and showing cropped image from selection. */
        

        /*Analysis of cropped image. */
        unsigned int idSelectedParcel =this->nbrOfSelections() - 1 ;
        // Creation of selected image .
        Mat croppedImage = this->getMainImage()->getMat()(selection).clone();
        
        // Creation of SelectedParcelAnalysis state.
        unique_ptr<SelectedParcelAnalysis> selectedParcelAnalysis = make_unique<SelectedParcelAnalysis>(this->getImageInfoExtractor() , idSelectedParcel , make_unique<Image>(croppedImage));
        
        unsigned int selectedParcelAnalysisId = State::registerState(move(selectedParcelAnalysis));
        State::execState(selectedParcelAnalysisId);




    /* END OF UNITARY TEST*/
    
    
    
    cout<<"MainParcelSelections execBehavior() : end"<<endl; 
}


unsigned int MainParcelSelections::nbrOfSelections()
{
	return this->mainParcelSelections.size();
} 

shared_ptr<Image> MainParcelSelections::getMainImage()
{
    return super::getMainImage();
}   

void MainParcelSelections::setImageWithSelections()
{   
    shared_ptr <Image> image = this->getMainImage();
        if(!image){cout<<"MainParcelSelections::setImageWithSelections() : No image found."<<endl;return;}

        if(!image->getMat().data){cout<<"MainParcelSelections::setImageWithSelections() : mat has no data."<<endl;return;}

    this->imageWithSelections = make_shared<Image> (this->getMainImage()->getMat().clone());
    
}


const shared_ptr<Image> MainParcelSelections::getImageWithSelections()
{
    return this->imageWithSelections;
}


void MainParcelSelections::setPointsForRotation()
{
    pointsForRotation = make_unique<vector<Point2i>>();   
}

void MainParcelSelections::addPointForRotation(Point2i &point)
{
    //Copying directly to heap allocated for the vector pointsForRotation
    //instead of allocating memory for each point. Since we only save 2 points
    //for rotation, it's preferred to not keep the points alive other than in the 
    //container.
    this->pointsForRotation->push_back(point);
    if(this->pointsForRotation->size() == 2)
    {
        this->rotateImage();
        this->pointsForRotation->clear();
    }
}



void MainParcelSelections::rotateImage()
{
    Point2i p1 = this->pointsForRotation->at(1);
    Point2i p0 = this->pointsForRotation->at(0);

    Point2i vp1 (p1.x - p0.x , p1.y - p0.y );
    double theta = atan2( vp1.y , vp1.x  ) * 180.0 / CV_PI;

    cout<<"MainParcelSelections::rotateImage() : Angle for rotation : "<< theta<<endl;

    // line(this->getMainImage()->getMat(), p0, p1, Scalar(255,0,0), 10, 8, 0);
    
    
    // line(this->getMainImage()->getMat(), Point2i(1000,1000), vp1 + Point2i(1000,1000), Scalar(255,0,0), 5, 8, 0);
    
    /*
    // get rotation matrix for rotating the image around its center in pixel coordinates
    Point2f center((this->getMainImage()->getMat().cols-1)/2.0, (this->getMainImage()->getMat().rows-1)/2.0);
    Mat rot = getRotationMatrix2D(center, theta, 1.0);
    // determine bounding rectangle, center not relevant
    Rect2f bbox = RotatedRect(Point2f(), this->getMainImage()->getMat().size(), theta).boundingRect2f();
    // adjust transformation matrix
    rot.at<double>(0,2) += bbox.width/2.0 - this->getMainImage()->getMat().cols/2.0;
    rot.at<double>(1,2) += bbox.height/2.0 - this->getMainImage()->getMat().rows/2.0;

    
    warpAffine(this->getMainImage()->getMat(), this->getMainImage()->getMat(), rot, bbox.size());
    // imwrite("rotated_im.png", dst);

    rotate2D(this->getMainImage()->getMat(), this->getMainImage()->getMat(),theta);
    */

    cout<<"MainParcelSelections::rotateImage() : refcount of old mat :"<<(this->getMainImage()->getMat().u->refcount)<<endl;

    Mat dst;

    Point2f center(this->getMainImage()->getMat().cols/2.0, this->getMainImage()->getMat().rows/2.0); 
    Mat rot = getRotationMatrix2D(center, theta, 1.0);
    Rect bbox = RotatedRect(Point2f(),this->getMainImage()->getMat().size(), theta).boundingRect2f();

    rot.at<double>(0,2) += bbox.width/2.0 - center.x;
    rot.at<double>(1,2) += bbox.height/2.0 - center.y;

    warpAffine(this->getMainImage()->getMat(), dst, rot, bbox.size());
    
    this->getMainImage()->setMat(dst);
    
    string winName ="Main Parcel Selections";
    TYPE_OF_VIEW_MANAGER * viewManager = static_cast<TYPE_OF_VIEW_MANAGER*>(this->getViewManager());
    viewManager->setWinName(winName);
    viewManager->setImageToDisplay(this->getMainImage());
    viewManager->displayImageFromMainThread();
        

} 






void MainParcelSelections::rotateImage(double theta)
{
    // theta = (double) theta * 180.0 / CV_PI;
    cout<<"MainParcelSelections::rotateImage() : Angle for rotation : "<< theta<<endl;

    // line(this->getMainImage()->getMat(), p0, p1, Scalar(255,0,0), 10, 8, 0);
    
    
    // line(this->getMainImage()->getMat(), Point2i(1000,1000), vp1 + Point2i(1000,1000), Scalar(255,0,0), 5, 8, 0);
    
    /*
    // get rotation matrix for rotating the image around its center in pixel coordinates
    Point2f center((this->getMainImage()->getMat().cols-1)/2.0, (this->getMainImage()->getMat().rows-1)/2.0);
    Mat rot = getRotationMatrix2D(center, theta, 1.0);
    // determine bounding rectangle, center not relevant
    Rect2f bbox = RotatedRect(Point2f(), this->getMainImage()->getMat().size(), theta).boundingRect2f();
    // adjust transformation matrix
    rot.at<double>(0,2) += bbox.width/2.0 - this->getMainImage()->getMat().cols/2.0;
    rot.at<double>(1,2) += bbox.height/2.0 - this->getMainImage()->getMat().rows/2.0;

    
    warpAffine(this->getMainImage()->getMat(), this->getMainImage()->getMat(), rot, bbox.size());
    // imwrite("rotated_im.png", dst);

    rotate2D(this->getMainImage()->getMat(), this->getMainImage()->getMat(),theta);
    */

    cout<<"MainParcelSelections::rotateImage() : refcount of old mat :"<<(this->getMainImage()->getMat().u->refcount)<<endl;

    Mat dst;

    Point2f center(this->getMainImage()->getMat().cols/2.0, this->getMainImage()->getMat().rows/2.0); 
    Mat rot = getRotationMatrix2D(center, theta, 1.0);
    Rect bbox = RotatedRect(Point2f(),this->getMainImage()->getMat().size(), theta).boundingRect2f();

    rot.at<double>(0,2) += bbox.width/2.0 - center.x;
    rot.at<double>(1,2) += bbox.height/2.0 - center.y;

    warpAffine(this->getMainImage()->getMat(), dst, rot, bbox.size());
    
    this->getMainImage()->setMat(dst);
    
    string winName ="Main Parcel Selections";
    TYPE_OF_VIEW_MANAGER * viewManager = static_cast<TYPE_OF_VIEW_MANAGER*>(this->getViewManager());
    viewManager->setWinName(winName);
    viewManager->setImageToDisplay(this->getMainImage());
    viewManager->displayImageFromMainThread();
        

} 