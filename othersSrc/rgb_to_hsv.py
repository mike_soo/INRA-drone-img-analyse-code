import numpy as np
import cv2

rawImage = cv2.imread('RGB/M2-5258-5518.tif')
cv2.imshow('Original Image',rawImage)
cv2.waitKey(1)

hsv = cv2.cvtColor(rawImage, cv2.COLOR_BGR2HSV)
cv2.imshow('HSV Image',hsv)
cv2.waitKey(0)