#ifndef DUMMY_TRACKBAR_HPP
#define DUMMY_TRACKBAR_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <iostream>
#include <string>

using namespace std;
using namespace cv;

/**
 * @brief      Class for parcel counting. Contains all essential attributs and
 *             method for the parcel automatic detection.
 */
class DummyTrackbar {
private:
	static int nbOfTrackbarsInCP;
public:
	static int d1;
	static string getNewNameOfTB();
	static void createDummyTrackbar();
};



#endif 
