#ifndef IMAGEF_FF_HPP
#define IMAGEF_FF_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <memory>

#include "Image.hpp"

using namespace cv;
using namespace std;






/**
 * @brief      ImageF class, standing for image for filering;  contains
 *             essential attributes and methods for the opencv::Mat instance
 *             manipulation requiring filtering applications.
 */
class ImageFF : public Image{ 
private:
	
	Mat filteredImageMat;
	using super = Image;
	vector <shared_ptr<Filter>> filters;
	double angleOfOrientation = 0.0;
public:
	ImageFF();
	ImageFF(Mat imageMat);
	~ImageFF();
	
	virtual Mat getFilteredImageMat() ; 
	virtual void addFilter(shared_ptr <Filter> filter);
	virtual void applyFilters();
	virtual void applyFilter(unsigned int i);
	
	virtual void setFilteredImageMat(Mat filteredImageMat);
	virtual void rotate(double angle);

	virtual void removeLineInFilteredImg(Point &p1 , Point &p2 , int connectivity = 8 ,int tickness = 3);
};

#endif  