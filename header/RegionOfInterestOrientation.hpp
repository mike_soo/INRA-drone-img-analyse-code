#ifndef MAIN_PARCEL_ORIENTATION_HPP
#define MAIN_PARCEL_ORIENTATION_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include <set>
#include <tbb/tbb.h>
#include "State.hpp"
#include "ImageFF.hpp"
#include "SelectedRegionOfInterestFiltering.hpp"
#include "DummyTrackbar.hpp"
using namespace tbb;
using namespace cv;
using namespace std;

#define NB_OF_ANGLE_CHOICES 5

class RegionOfInterestOrientation;

/**
 * @brief      Class for main parcel orientation button callback parameters.
 *             Used to pass parameters of rotation details to the callback
 *             function used when an event (click or button push) is detected.
 */
class RegionOfInterestOrientationButtonCallbackParams{
public:
	RegionOfInterestOrientation * regionOfInterestOrientation;
	double angleOfRotation;
};


/**
 * @brief      Class for main parcel orientation state. Retains attributes and
 *             methods for automatic orientation of zones of interest.
 */
class RegionOfInterestOrientation : public State{

private:
	using super = State;
	
	int parcelOrientationAnalysisId = - 1;
	RegionOfInterestOrientationButtonCallbackParams regionOfInterestOrientationCallbackParams[NB_OF_ANGLE_CHOICES];
	
	void execBehavior();
	void init();
public:
	unsigned int parcelCounterId = 0;
	string winName;
	void setParcelOrientationAnalysisId(int id);
	int getParcelOrientationAnalysisId();
	shared_ptr<ImageFF> imageToOrientateOrig , imageToOrientate , imageToShow;
	int angleOfImageWithSelections=0;
	RegionOfInterestOrientation(shared_ptr<ImageInfoExtractor>  imageInfoExtractor);
	~RegionOfInterestOrientation();
	void setRegionOfInterestOrientation();
	void setImageToOrientate(shared_ptr <ImageFF> imageToOrientate);		

};

void rotateButtons(int , void * userdata );
void okrotationButton(int , void * regionOfInterestOrientationPtr);

#endif 
