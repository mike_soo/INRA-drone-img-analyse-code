#ifndef SELECTING_REGIONS_OF_INTEREST_HPP
#define SELECTING_REGIONS_OF_INTEREST_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include "State.hpp"
#include "Image.hpp"
#include "RegionOfInterestOrientation.hpp"
#include "FilteringForOrientation.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for selecting regions of interest intented to be analyse
 *             for automatic field parcels detection.
 */
class SelectingRegionsOfInterest : public State{

private:
	using super = State;
	virtual void execBehavior();
	void init();	
	int numberOfSelectedROIs = 0;
public:
	bool ROI = false;
	shared_ptr<Image> colorImageOrig , colorImageWithROIs;
	vector<Point>pointsOfRegionOfInterest;
	SelectingRegionsOfInterest(shared_ptr<ImageInfoExtractor>  imageInfoExtractor);
	
	int getNewIdForSelectedRoi();
	void setColorImage(shared_ptr<Image> colorImage );
};

#endif 
