#ifndef LINE_HPP
#define LINE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <memory>
#include <cmath>
#include <set>
#include <map>
#include "State.hpp"
#include "ImageFF.hpp"
#include "DrawableGeometStruct.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      The Line class, a geometric structure useful for Image's matrix
 *             data modification.
 */
class Line : public DrawableGeometStruct
{
private:
	using super = DrawableGeometStruct;
	Point p1 , p2;
public:
	Line();
	Line(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF , int x1 , int y1 , int x2 , int y2);
	void drawC(int r , int g , int b);
	void drawF();
	friend ostream &operator<<( ostream &output, Line &z );
	int getX1();
	int getY1();
	int getX2();
	int getY2();
	bool operator ==(Line &otherLine);
	bool operator !=(Line &otherLine);
	double eucledianDist();

	void setp1p2coords(int x1 , int y1 , int x2 , int y2);
	
};
#endif 

