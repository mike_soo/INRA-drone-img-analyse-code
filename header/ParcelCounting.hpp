#ifndef PARCEL_COUNTING_HPP
#define PARCEL_COUNTING_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <memory>
#include <cmath>
#include <set>
#include "Zone.hpp"
#include "State.hpp"
#include "Image.hpp"
#include "Line.hpp"
#include "DummyTrackbar.hpp"
using namespace cv;
using namespace std;



/**
 * @brief      Class for parcel counting. Contains all essential attributs and
 *             method for the parcel automatic detection.
 */
class ParcelCounting : public State{

private:
	bool horFirst = true;
	using super = State;
	virtual void init();
	virtual void execBehavior();
	bool horCountingInProg = true , verCountingInProg = false;
	vector<int> nbOfHorizontalBorderPixelsByZone;
		
	int horizontalParcelGroupAnalysisId=0;
	vector<shared_ptr<Zone>> horizontalZonesWithMaxPixels;


	vector <shared_ptr<Zone>> verticalZonesWithMaxPixels;
	vector<vector <shared_ptr<Zone>>> verticalZonesWithMaxPixelsByHorId;
	// Analyzing a horizontally grouped set of parcels, we define the next
	// vector containing, for each horizontal group identified by an id,
	// the maximum value of pixels contained in each vertical zone identifying the  
	// vertical border of each parcel in the set. 
	vector<vector<int>> horParcelGroupMaxPixVertBord ;


	map<string , shared_ptr<Zone> > verticalBordersConfirmed , horizontalBordersConfirmed;
	
	void horizontalBoundaryAnalysis(int iDelimiter1 , int iDelimiter2 , int verticalBorderError);

	void addNewHorBorderConfirmed(shared_ptr<Zone> zone);
	void addNewHorBoundaryZoneConfirmed(shared_ptr<Zone> horBoundaryZone);
	void addNewHorBoundaryZoneConfirmed(Zone& zone)	;

public:
	bool zonesForVertDistDeducPriority=false;
	string winNameOfImageToAnalyse;
	map<string , shared_ptr<Zone> > parcelZones , horBoundaryZones;
	ParcelCounting(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , shared_ptr<ImageFF> imageFF);
	// Vector containing points of a model parcel useful for searching all existing parcels.
	vector <Point> pointsOfModelParcel;  

 	shared_ptr<ImageFF> origImageFF , drawableImageFF;
 	void setOrigImage(shared_ptr <ImageFF> origImageFF);
 	
	bool pUserHorDist1set=false , pUserVertDist1set=false;
	//TODO : convert zoneiForHorDistDeduc to shared_ptr<Zone> pointers
	Zone *zone1ForHorDistDeduc  = nullptr , *zone2ForHorDistDeduc  = nullptr ;
	shared_ptr<Zone> zone1ForVertDistDeduc , zone2ForVertDistDeduc; 
	Line modelLineHorDistParcel , modelLineVerDistParcel;
	int horDistParcelSimilarnessInt = 890 , verDistParcelSimilarnessInt = 890;

	int verticalBorderError=3, horizontalBorderError =10 , maxBorderError = 50;
	int hZonesSimilarnessInt = 665 , vZonesSimilarnessInt = 500 , vBorderSimilarnessInt = 500 , maxVertBorderOverLappingDistInt= 300, maxHorBorderOverLappingDistInt= 300;
	const double maxCoeff = 1000;
	
	void activateHorCounting();
	void activateVerCounting();
	
	bool horCountingActive();
	bool verCountingActive();
	
	void horizontalCounting(int verticalMarginError);
	void verticalCounting(int iDelimiter1 , int iDelimiter2 , int horizontalMarginError);
	
	


	// Will attempt to remove vertical zones that doesn't contains a vertical border of a parcel.
	void filterVertZonesByMaxColsQtySimilarness();
	
	
	void searchHorBoundaries();
	void horizontalBoundaryAnalysis();
	void searchParcelZones();

	void verticalBordersConfirmedClear();
	
	void verticalBordersDeductionsClear();

	void horizontalZonesClear();
	void horizontalDeductionsClear();

	Zone* getVZone(Point &p);
	shared_ptr<Zone>  getHZone(Point &p);

	void drawConfirmedVerticalZones();
	void drawConfirmedHorizontalZones();
	
	
	void drawParcelZones();
	void drawHorBoundaries();

	void colorDrawableImageReset();

	void verZonesClear();
	bool inParcelZone(int x , int y);
	shared_ptr<Zone> getParcelZone(int x , int y);
	shared_ptr<Zone> getHorBoundaryZone(int x , int y);

	bool isVertZoneInConfirmedPZone(Zone &zone);
	bool isHorZoneInConfirmedBoundaryZone(Zone &zone);
	
	bool isAConfirmedHorizontalZone(Zone &zone);

	shared_ptr<Zone> getHZoneConfirmed(string zoneKey);
	

	bool createIfValidNewHorBoundaryZone(shared_ptr<Zone> horZone1 , shared_ptr<Zone> horZone2);



};

void onMouseParcelCounting( int event, int x, int y, int, void* parcelCountingPtr);
void parcelHorizontalCountingStart(int , void *userdata);

void repeatVerticalCounting(int , void *parcelCountingPtr);
void repeatHorizontalCounting(int , void *parcelCountingPtr);

void verBordersOk(int , void *parcelCountingPtr);
void horBordersOk(int , void *parcelCountingPtr);



#endif 
