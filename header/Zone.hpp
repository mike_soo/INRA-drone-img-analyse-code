#ifndef ZONE_HPP
#define ZONE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <memory>
#include <cmath>
#include <set>
#include <map>
#include "State.hpp"
#include "Image.hpp"
#include "ImageFF.hpp"
#include "DrawableGeometStruct.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for a geometrical mostly rectangular (can be extended) form
 *             representation used mainly as a structure for parcel borders
 *             analysis. Used to retain information such as border pixel
 *             quantity, parcel location, and for visual representation
 *             purposes.
 */
class Zone : public DrawableGeometStruct
{
private:
	using super = DrawableGeometStruct;
	int colIndexWithMaxPixels=-1 , colWithMaxPixelsQty = -1;
	int nbOfPixels= -1;
	Rect zone;	
public:
	Zone();
	Zone(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF , int x , int y , int width , int height);
	
	int getColWithMaxPixelsQty();
	int getColIndexWithMaxPixels();

	

	void findColWithMaxPixelsQty();
	void drawColWithMaxPixelQtyC(int r , int g , int b);
	int getNbOfPixels();
	
	void drawC(int r , int g , int b);
	void drawF();
	bool operator <( Zone & otherZone);
	friend ostream &operator<<( ostream &output, Zone &z );
	int getX();
	int getY();
	int getW();
	
	void drawHorInBetweenSurface(Zone &zone , int vertSpacing);
	int getH();
	bool operator ==(Zone &otherZone);
	bool operator !=(Zone &otherZone);
	string coordsToString();
	void setXYWH(int x , int y , int width , int height);

	string toString();

	
};

/**
 * @brief      Structure used to allow std::sort usage.
 */
struct SortByNbOfPixels{

	/**
	 * brief Operator() function for std::sort(std,std) usage.
	 * @details    Allows sorting function usage by number of pixels contained in each zone.
	 * @param[in] zone1 used for comparison in std::sort function.
	 * @param[in] zone2 used for comparison in std::sort function.
	 */
	bool operator() (shared_ptr<Zone>& zone1 , shared_ptr<Zone>& zone2 );
};

/**
 * @brief      Structure used to allow std::sort usage.
 */
struct SortByColWithMaxPixels{

	/**
	 * brief Operator() function for std::sort(std,std) usage.
	 * @details    Allows sorting function usage by number of pixels contained
	 *             in the zone column intersecting the highest numbers of
	 *             vertical aligned pixels.
	 * @param[in]  zone1  used for comparison in std::sort function.
	 * @param[in]  zone2  used for comparison in std::sort function.
	 */
	bool operator() (shared_ptr<Zone>& zone1 , shared_ptr<Zone>& zone2 );
};


/**
 * @brief      Structure used to allow std::sort usage.
 */
struct SortByCoords{
	/**
	 * brief Operator() function for std::sort(std,std) usage.
	 * @details    Allows sorting function usage by the zones coordinates value where zone1 < zone2 if and only if zone1.x < zone2.x || zone1.y < zone2.y.  
	 * @param[in]  zone1  used for comparison in std::sort function.
	 * @param[in]  zone2  used for comparison in std::sort function.
	 */
	bool operator() (shared_ptr<Zone>& zone1 , shared_ptr<Zone>& zone2 );
	
};




#endif 

