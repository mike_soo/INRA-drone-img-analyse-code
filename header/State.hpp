#ifndef STATE_HPP
#define STATE_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <tbb/tbb.h>
#include <tbb/compat/condition_variable>
#include <iostream>
#include <memory>
#include <map>

using namespace cv;
using namespace std;
class ImageInfoExtractor;
class Image;

/**
 * @brief      Base class imposing a structure of how each image 
 *             process should be implemented. Responsible also of the
 *             creation and execution of each desired state. In other words this
 *             class works as the main structure for each created procedure and
 *             as state class factory.
 */
class State{
 
public:
	State(shared_ptr <ImageInfoExtractor> imageInfoExtractor);
	bool stayAlive = true;
	enum StateOfImageExtractor{
		MAIN_PARCEL_SELECTIONS,
		SELECTED_PARCEL_ANALYSIS,
		MAIN_PARCEL_ORIENTATION
	};

	static unsigned int ids;
	static unsigned int registerState(unique_ptr<State> state);
	static int execState(unsigned int stateId);
	static State* get(unsigned int stateId);
	
	virtual shared_ptr<ImageInfoExtractor> getImageInfoExtractor();
	virtual shared_ptr<Image> getMainImage();
	virtual void stop();
	virtual void setStateName(string &stateName);
	virtual string getStateName();
	virtual void setId(unsigned int id);
	virtual unsigned int getId();
	virtual void setMainImage(shared_ptr<Image> image);
	virtual shared_ptr<const Image> getOriginalImage();
private:
	static unsigned int getNewId();
	virtual void exec();
	static tbb::mutex activeStateMutex;
	static map <unsigned int , unique_ptr<State>> activeStates;
	unsigned int id;
	shared_ptr<ImageInfoExtractor> imageInfoExtractor;
	string stateName="";
	virtual void execBehavior()=0;
	virtual void init()=0;
};

#endif 
