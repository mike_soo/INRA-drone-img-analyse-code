#ifndef MICRO_PARCEL_EXTRACTOR_HPP
#define MICRO_PARCEL_EXTRACTOR_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <memory>
#include "State.hpp"
#include "Image.hpp"


using namespace cv;
using namespace std;
class State;

/**
 * @brief      This class is supposed to know the context common to every
 *             state. It mainly holds the first and original image to be
 *             analyzed.
 * @details    Every state includes an attribute pointing at the main
 *             ImageInfoExtractor where the quantity of this class active
 *             instances should be limited to one for each orthoimage to
 *             analyze.
 */
class ImageInfoExtractor{

private:
	shared_ptr <Image> image;
	shared_ptr<const Image> originalImage;
	
public:
	
	virtual void setMainImage(Image* image);
	virtual void setMainImage(shared_ptr <Image> image);
	virtual shared_ptr<Image> getMainImage();
	virtual shared_ptr<const Image> getOriginalImage();
	
};

#endif 
