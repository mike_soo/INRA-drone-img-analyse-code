#ifndef GEOMETSTRUCT_HPP
#define GEOMETSTRUCT_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <memory>
#include <cmath>
#include <set>
#include <map>
#include "State.hpp"
#include "ImageFF.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for a drawable geometric structure. Includes several
 *             methods and attributes for the common task of drawing geometric
 *             structures in color and gray scaled Image typed images.
 *             This base class factorizes the emplacements used of the 
 *             attributes and drawing methods.
 */
class DrawableGeometStruct {

private:
public:
	shared_ptr<ImageFF> origImageFF ;
	shared_ptr<ImageFF> drawableImageFF ;
	
	DrawableGeometStruct();
	DrawableGeometStruct(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF );

	void setImages(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF );
	virtual void drawC(int r , int g , int b)=0;
	

	
};

#endif 

