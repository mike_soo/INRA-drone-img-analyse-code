#ifndef FILTERING_FOR_PARCELCOUNTING_HPP
#define FILTERING_FOR_PARCELCOUNTING_HPP

#include "SelectedRegionOfInterestFiltering.hpp"
#include "RegionOfInterestOrientation.hpp"

/**
 * @brief      Class for the selected region of interest filtering process.
 */
class FilteringForParcelCounting : public SelectedRegionOfInterestFiltering
{

private:
	using super = SelectedRegionOfInterestFiltering;
	virtual void execBehavior();
	virtual void init();
public:
	unsigned int roiParcelCounterId = 0;
	FilteringForParcelCounting(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , unsigned int idSelectedRegionOfInterest , shared_ptr<Image> selectedRegionOfInterest);	
};


void filteringForOrientationOkButton(int , void * filteringForOrientationPtr);
#endif 
