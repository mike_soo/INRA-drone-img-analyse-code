#ifndef FILTER_HPP
#define FILTER_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <memory>

using namespace cv;
using namespace std;
class ImageFF;

/**
 * @brief      Base class for filter implementation.
 */
class Filter{

private:
	virtual void applyFilter(ImageFF* image)=0;
protected:
	ImageFF* image = nullptr;
public:
	
	
	virtual void applyFilter();

	virtual ImageFF* getImage();
	virtual void setImage(ImageFF* image);
};

#endif 
