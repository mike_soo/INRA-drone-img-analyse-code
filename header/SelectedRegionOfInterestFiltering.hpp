#ifndef SELECTED_PARCEL_ANALYSIS_HPP
#define SELECTED_PARCEL_ANALYSIS_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include "State.hpp"
#include "ImageFF.hpp"
#include "CannyFilter.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for the selected region of interest filtering process.
 */
class SelectedRegionOfInterestFiltering : public State{

private:
	using super = State;
	
	
	unsigned int idSelectedRegionOfInterest;
	unsigned int selectedRegionOfInterestCounterId = 0;
public:
	Point pointsOfRegionOfInterest[4];
	shared_ptr<ImageFF> selectedRegionOfInterest;
	string winNameOfImageToAnalyse;
	SelectedRegionOfInterestFiltering(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , unsigned int idSelectedRegionOfInterest , shared_ptr<Image> selectedRegionOfInterest);
	~SelectedRegionOfInterestFiltering();
	virtual unsigned int getIdOfSelectedRegionOfInterest();
	
	
	
};
void filtering(int , void* selectedRegionOfInterestFilteringPtr);

#endif 
