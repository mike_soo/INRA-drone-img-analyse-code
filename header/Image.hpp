#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <memory>
// #include <tbb/tbb.h>
#include "Filter.hpp"

using namespace cv;
using namespace std;






/**
 * @brief      Image class contains essential attributes and methods for the
 *             opencv::Mat instance manipulation.
 */
class Image{
private:
	Mat imageMat;
	Mat filteredImageMat;

	vector <shared_ptr<Filter>> filters;
		
	vector <Point> roiBorderPoints;
	double angleOfOrientation = 0.0;
	virtual void lookForBorderPoints();
protected:
	void rotate(double angleOfRotation , Mat &imageMat);
public:
	Image();
	Image(Mat mat);
	~Image();
	virtual Mat  getImageMat(); 
	virtual void setImageMat(Mat imageMat);
	
	virtual void rotate(double angle);

	virtual double getAngleOfOrientation();
	virtual void drawPointOfInterest(Point &poi , Scalar &color);
	virtual void drawPoint(Point &poi , const Vec3b & color);
	virtual void addROIBorderPoint(Point poi);
	virtual void drawLine( const Point &p1 , const Point &p2 , const Scalar &color);
	virtual void drawGrid();
	virtual void drawHorGrid(const int horSpacing , const Scalar color );
	virtual void drawVerGrid(const int verSpacing , const Scalar color);
};

#endif  