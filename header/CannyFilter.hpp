#ifndef CANNY_FILTER_HPP
#define CANNY_FILTER_HPP
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "Filter.hpp"
#include "ImageFF.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for canny filter containing essential attributes and
 *             methods for the filtering procedure.
 */
class CannyFilter : public Filter
{

private:
	using super = Filter;
	
	virtual void applyFilter(ImageFF* wimage);

public:
	int edgeThresh = 1;
	int lowThreshold = 82;
	int const max_lowThreshold = 200;
	int ratio = 2;
	int kernel_size = 3;
	Point borderPoints[4];
	virtual void setImage(ImageFF* image);
	void lookForBorderPoints();


};

#endif 