#ifndef GAUSSIANFILTER_HPP
#define GAUSSIANFILTER_HPP
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "Filter.hpp"
#include "ImageFF.hpp"
using namespace cv;
using namespace std;

/**
 * @brief      Class for gaussian filter containing essential attributes and
 *             methods for the filtering procedure.
 */
class GaussianFilter : public Filter
{

private:
public:
	virtual void applyFilter(Image* image);


};

#endif 