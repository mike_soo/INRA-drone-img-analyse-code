var searchData=
[
  ['operator_21_3d',['operator!=',['../classLine.html#ac2125e0841e7307674a1df124506fae3',1,'Line::operator!=()'],['../classZone.html#a0075259e5c39785d98c12e1c6ca50416',1,'Zone::operator!=()']]],
  ['operator_28_29',['operator()',['../structSortByNbOfPixels.html#abf08c7ecc19975fb07ecd80f51b84a70',1,'SortByNbOfPixels::operator()()'],['../structSortByColWithMaxPixels.html#ac1cced610cd4380be29f6c5509502bfe',1,'SortByColWithMaxPixels::operator()()'],['../structSortByCoords.html#ad005389272b7223ef4895d6206a5d0cf',1,'SortByCoords::operator()()']]],
  ['operator_3c',['operator&lt;',['../classZone.html#a750a0534d84ea17dde604402648f1dfb',1,'Zone']]],
  ['operator_3d_3d',['operator==',['../classLine.html#ad076d2fa285b05197db2caaa34a12444',1,'Line::operator==()'],['../classZone.html#ac19ec6806780eb9d6511984155757996',1,'Zone::operator==()']]]
];
