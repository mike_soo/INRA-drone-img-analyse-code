Welcome to the project of automatic field parcel detection.

To get access to the generated documentation open in your favorite navigator the file docs/html/index.html.

A manual of utilisation is avaible [here](https://docs.google.com/presentation/d/1GnXy4udxbQx7pcQyWRyVYr5POVkwn-HwokEFjILD150/edit?usp=sharing).

Regards.

Michael O.  @ INRA CAPA - LIRMM