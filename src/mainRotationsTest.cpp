#include <opencv2/core/core.hpp>				
#include <opencv2/highgui/highgui.hpp>
#include <iostream>


#include "Image.hpp"
#include "CannyFilter.hpp"
using namespace cv;
using namespace std;

void rotate(Point &p , Point &center , double a)
{
	Point tempP;
	tempP.x = ((p.x - center.x) * cos(a)) - ((p.y - center.y) * sin(a)) + center.x;
	tempP.y = ((p.x - center.x) * sin(a)) + ((p.y - center.y) * cos(a)) + center.y;

	p.x = tempP.x;
	p.y = tempP.y;
    
}

int main( int argc, char** argv )
{
    if( argc != 2)
    {
     cout <<" Usage: parameters ImageToLoadAndDisplay" << endl;
     return -1;
    } 

    
    Mat imageMat = Mat(imread(argv[1],IMREAD_COLOR));
    if(! imageMat.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    
    namedWindow("Main image", WINDOW_NORMAL);
    resizeWindow("Main image", 600, 600);
    imshow("Main image", imageMat);    
    
    cout<<"main rotation test runing"<<endl;
	
	// Rect oRect = Rect( 0 , 0 , imageMat.cols , imageMat.rows);
	Mat dstRot , dstCrop;

	Mat mask(imageMat.rows, imageMat.cols, CV_8UC3, cv::Scalar(0 , 0 , 0));
	vector< vector<Point> >  co_ordinates;
	vector <Point> pointsOfRegionOfInterest;
	Point p1(507  , 4698), 
	      p2(1044 , 5187),
	      p3(4428 , 1459),
	      p4(3887 , 1002);

	pointsOfRegionOfInterest.push_back(p1);
	pointsOfRegionOfInterest.push_back(p2);
	pointsOfRegionOfInterest.push_back(p3);
	pointsOfRegionOfInterest.push_back(p4);



	co_ordinates.push_back(pointsOfRegionOfInterest);
	
	// Creates mask where the polygon created by 4 user clicks will be stored.
	drawContours( mask,co_ordinates,0, Scalar(255 , 255 , 255),CV_FILLED, 8 );

	bitwise_and(mask , imageMat , dstCrop);

	namedWindow("Main image cropped", WINDOW_NORMAL);
    resizeWindow("Main image cropped", 600, 600);
	imshow("Main image cropped" , dstCrop);

	shared_ptr<ImageFF> image= make_shared<ImageFF>(dstCrop);
    shared_ptr<CannyFilter> cannyFilter = make_shared<CannyFilter>();

    image->addFilter(cannyFilter);
    cannyFilter->borderPoints[0] = p1;
    cannyFilter->borderPoints[1] = p2;
    cannyFilter->borderPoints[2] = p3;
    cannyFilter->borderPoints[3] = p4;
    image->applyFilters();

    Point center(dstCrop.cols / 2.0 , dstCrop.rows / 2.0);

    double a = 48.0 *M_PI / 180.0;

    

    
    namedWindow("Canny", WINDOW_NORMAL);
    resizeWindow("Canny", 600, 600);
    imshow("Canny", image->getImageMat());
    





 
 	//Calculation of the current image center that will be used as the center of rotation
	// Point2f center(image->getImageMat().cols/2.0 , image->getImageMat().rows/2.0); 
	// double angleOfRotation = 20.0 ;
	// //Calculation of the imageMatrix of rotation. 
	// Mat rot = getRotationMatrix2D(center, angleOfRotation, 1.0);
	// RotatedRect rRect = RotatedRect(center , image->getImageMat().size() ,  angleOfRotation);

	// Rect bRect = rRect.boundingRect2f();

	// warpAffine(image->getImageMat(), dstRot, rot, bRect.size());
	// image->setMat(dstRot);

	
	
 //    Point2f vertices[4];
 //    rRect.points(vertices);

 //    for (int i = 0; i < 4; i++)
 //    {
 //    	cout<<"vertices "<<i<<" : "<<vertices[i]<<endl;
 //        line(imageMat, vertices[i], vertices[(i+1)%4], Scalar(0,255,0), 2);
 //    }
    
 //    rectangle(imageMat , oRect , Scalar(0 , 0 , 255) , 10);
 //    image->rotate(20);
    
 //    rectangle(imageMat , bRect, Scalar(255 , 0 , 0), 10);
 //    namedWindow("rectangles", WINDOW_NORMAL);
 //    resizeWindow("rectangles", 600, 600);
 //    imshow("rectangles", imageMat);
    
    
    // Infinite loop managing calls for images display.
    while(1)
    {
        waitKey(0);   
    }
    
    
    return 0;
}
