#include "DrawableGeometStruct.hpp"


/**
 * @brief      Constructs the object.
 */
DrawableGeometStruct::DrawableGeometStruct(){}

/**
 * @brief      Constructs the object with both original image (for analysing
 *             purpose) and drawable image. Image are
 *             used to receive geometric drawn forms.
 *
 * @details    Sets the images that will be used for drawing results and their
 *             original respective versions for images reinitialization and
 *             analysis .
 *
 * @param[in]  origImageFF      The original ImageFF
 * @param[in]  drawableImageFF  The drawable ImageFF
 */
DrawableGeometStruct::DrawableGeometStruct(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF )
{
	this->origImageFF = origImageFF; 
	this->drawableImageFF = drawableImageFF; 
}


/**
 * @brief      Constructs the object with both original image (for analysing
 *             purpose) and drawable image. Image are used to receive geometric
 *             drawn forms.
 *
 * @details    Sets the images that will be used for drawing results and their
 *             original respective versions for images reinitialization and
 *             analysis .
 *
 * @param[in]  origImageFF      The original ImageFF
 * @param[in]  drawableImageFF  The drawable ImageFF
 */
void DrawableGeometStruct::setImages(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF )
{
	this->origImageFF = origImageFF; 
	this->drawableImageFF = drawableImageFF; 
}


