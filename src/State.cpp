#include "State.hpp"
#include "ImageInfoExtractor.hpp"

map <unsigned int , unique_ptr<State>> State::activeStates;
tbb::mutex State::activeStateMutex;
unsigned int State::ids = 1;



/**
 * @brief      Constructs the object.
 *
 * @param[in]  imageInfoExtractor  The image information extractor
 */
State::State(shared_ptr <ImageInfoExtractor> imageInfoExtractor)
{
	this->imageInfoExtractor = imageInfoExtractor;
	
}

/**
 * @brief      Return a state pointer pointing to the selected (by id) state's
 *             instance.
 *
 *
 * @param[in]  stateId  The state identifier
 *
 * @return     { description_of_the_return_value }
 */
State* State::get(unsigned int stateId)
{	
	map<unsigned int , unique_ptr<State>>::iterator it = State::activeStates.find(stateId);

	if(it != State::activeStates.end())
	{
		return it->second.get();
	}
	return nullptr;

}

/**
 * @brief      Register a state recently instaciated. 
 *
 * @param[in]  state  The state
 *
 * @return     return state id
 */
unsigned int State::registerState(unique_ptr<State> state)
{
	if(!state)
		return -1;

	State::activeStateMutex.lock();
	unsigned int stateId= State::getNewId();
	state->setId( stateId);
	state->init();
	State::activeStates.insert(make_pair(state->getId() , move(state)));

	State::activeStateMutex.unlock();
	return stateId;
}


/**
 * @brief      Sets the state name.
 *
 * @param      stateName  The state name
 */
void State::setStateName(string &stateName) 
{
	this->stateName = stateName;
}


/**
 * @brief      Gets the state name.
 *
 * @return     The state name.
 */
string State::getStateName()
{
	return this->stateName;	
}
 


/**
 * @brief      Gets the image information extractor.
 *
 * @return     The image information extractor.
 */
shared_ptr<ImageInfoExtractor> State::getImageInfoExtractor()
{
	return this->imageInfoExtractor;
}



/**
 * @brief      Gets the main image accessible by all the active states.
 *
 * @return     The main image.
 */
shared_ptr<Image> State::getMainImage()
{
	shared_ptr <ImageInfoExtractor> imageInfoExtractor=this->getImageInfoExtractor();
	if(imageInfoExtractor)
	{
		return imageInfoExtractor->getMainImage();
	}
	return shared_ptr<Image> (nullptr);
}


/**
 * @brief      Gets the original image accessible by all the active state.
 *
 * @return     The original image.
 */
shared_ptr<const Image> State::getOriginalImage()
{
	shared_ptr <ImageInfoExtractor> imageInfoExtractor=this->getImageInfoExtractor();
	if(imageInfoExtractor)
	{
		return imageInfoExtractor->getOriginalImage();
	}
	return shared_ptr<Image> (nullptr);	
}


/**
 * @brief      Generic exec of every state.
 * @details    All generic procedure asked to be executed along the state
 *             behavior should be implemented here.
 */
void State::exec()
{
	this->execBehavior();
}


/**
 * @brief      Exit procedure to be called once the state is removed from
 *             the activeStates map structure. 
 */
void State::stop()
{
}


/**
 * @brief      Gets the new identifier for a newly created state.
 *
 * @return     The new identifier.
 */
unsigned int State::getNewId()
{
	return State::ids++;
}


/**
 * @brief      Sets the state identifier.
 *
 * @param[in]  id    The identifier
 */
void State::setId(unsigned int id)
{
	this->id = id;
}


/**
 * @brief      Gets the state identifier.
 *
 * @return     The identifier.
 */
unsigned int State::getId()
{
	return this->id;
}


/**
 * @brief      Executes the desired id identified state.
 *
 * @param[in]  stateId  The state identifier
 *
 * @return     1 if the state was found in the activeStates maps and therefore
 *             executed, -1 otherwise.
 */
int State::execState(unsigned int stateId)
{
	map<unsigned int , unique_ptr<State>>::iterator it = State::activeStates.find(stateId);
	if(it != State::activeStates.end())
	{
		it->second->exec();
		return 1;
	}
	return -1;
	
}

/**
 * @brief      Sets the main image accessible from all the active states.
 *
 * @param[in]  image  The main image
 */
void State::setMainImage(shared_ptr <Image> image)
{
	shared_ptr <ImageInfoExtractor> imageInfoExtractor=this->getImageInfoExtractor();
	if(imageInfoExtractor)
	{
		imageInfoExtractor->setMainImage(image);
	}
	else
	{
		cerr<<"State::setMainImage(shared_ptr) : imageInfoExtractor == null "<<endl;
	}
}