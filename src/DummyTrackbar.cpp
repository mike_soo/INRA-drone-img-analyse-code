#include "DummyTrackbar.hpp"


int DummyTrackbar::d1 = 1;
int DummyTrackbar::nbOfTrackbarsInCP = 0 ;


string DummyTrackbar::getNewNameOfTB()
{
	DummyTrackbar::nbOfTrackbarsInCP++;
	return to_string(DummyTrackbar::nbOfTrackbarsInCP);

}

void DummyTrackbar::createDummyTrackbar()
{
	createTrackbar(DummyTrackbar::getNewNameOfTB(), "", &DummyTrackbar::d1 , 1 , NULL);  

}