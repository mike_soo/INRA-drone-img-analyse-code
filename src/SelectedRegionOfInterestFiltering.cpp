#include "SelectedRegionOfInterestFiltering.hpp"
#include "ImageInfoExtractor.hpp"
#include "ParcelCounting.hpp" 

/**
 * @brief      Constructs the object.
 *
 * @param[in]  imageInfoExtractor  The image information extractor
 * @param[in]  idSelectedRegionOfInterest    The identifier selected parcel
 * @param[in]  selectedRegionOfInterest      The selected parcel
 */
SelectedRegionOfInterestFiltering::SelectedRegionOfInterestFiltering(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , unsigned int idSelectedRegionOfInterest , shared_ptr<Image> selectedRegionOfInterest) : State(imageInfoExtractor)
{
    this->idSelectedRegionOfInterest = idSelectedRegionOfInterest;
    this->selectedRegionOfInterest = make_shared<ImageFF>(selectedRegionOfInterest->getImageMat());
    
    string stateName = "SelectedRegionOfInterestFiltering";
    this->setStateName( stateName );

}

SelectedRegionOfInterestFiltering::~SelectedRegionOfInterestFiltering()
{
    cout<<this->getStateName()<<"::~SelectedRegionOfInterestFiltering()"<<endl;
}


 



/**
 * @brief      Gets the identifier of the selected region of interest.
 *
 * @return     The identifier of selected region of interest.
 */
unsigned int SelectedRegionOfInterestFiltering::getIdOfSelectedRegionOfInterest()
{
    return this->idSelectedRegionOfInterest;
}


/**
 * @brief      Callback function charged of the selected region of interest
 *             canny filtering..
 *
 * @param[in]  <unnamed>                             0
 * @param      selectedRegionOfInterestFilteringPtr  A void* pointer pointing to
 *                                                   a
 *                                                   SelectedRegionOfInterestFiltering
 *                                                   instance
 */
void filtering(int , void* selectedRegionOfInterestFilteringPtr)
{
    SelectedRegionOfInterestFiltering *selectedRegionOfInterestFiltering = (SelectedRegionOfInterestFiltering*) selectedRegionOfInterestFilteringPtr;

    
    if(selectedRegionOfInterestFiltering != nullptr)
    {    
        
        selectedRegionOfInterestFiltering->selectedRegionOfInterest->applyFilters();
        
        imshow(selectedRegionOfInterestFiltering->winNameOfImageToAnalyse , selectedRegionOfInterestFiltering->selectedRegionOfInterest->getFilteredImageMat());
     

    }
}

