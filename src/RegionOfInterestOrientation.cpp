#include "RegionOfInterestOrientation.hpp"
#include "ImageInfoExtractor.hpp"   
#include "ParcelCounting.hpp"

/**
 * @brief      Constructs the object.
 *
 * @param[in]  imageInfoExtractor  The image information extractor
 */
RegionOfInterestOrientation::RegionOfInterestOrientation(shared_ptr <ImageInfoExtractor> imageInfoExtractor) : State(imageInfoExtractor)
{
    
}


RegionOfInterestOrientation::~RegionOfInterestOrientation()
{
    cout<<this->getStateName()<<"::~RegionOfInterestOrientation()"<<endl;
}


/**
 * @brief      Initialization of this instance. Called first and only once when
 *             this state is executed
 */
void RegionOfInterestOrientation::init()
{
  string stateName="RegionOfInterestOrientation";
  this->setStateName(stateName); 
  this->winName= "Setting orientation of the ROI " + to_string(this->getParcelOrientationAnalysisId());
  imageToShow = make_shared<ImageFF> ();
}


/**
 * @brief      Execute behavior of this state.
 * 
 * @details    Procedure for setting the selected region of interest is invoked.
 */
void RegionOfInterestOrientation::execBehavior()
{
  this->setRegionOfInterestOrientation();



}




void RegionOfInterestOrientation::setRegionOfInterestOrientation()
{

  vector<Vec4i> lines;
  vector<int> angles;
  const int randianAnglesResolution = 180;
  const int decimalAnglesResolution = 10.0;
  cout.precision(64);
  // Calling hough procedure detecting existing lines patterns in the image to orientate
  HoughLinesP(this->imageToOrientateOrig->getFilteredImageMat() , lines, 1, (double)CV_PI/randianAnglesResolution, 5, 10, 1 );
  
  // Calculating angle of each founded line.
  double theta;
  int thetaInt;
  // Discretization of angles histogram.
  unsigned anglesCumulativesInts[361]={};
  for(unsigned int i = 0 ; i < lines.size() ; i++)
  { 

    Point2i vp1 (lines.at(i)(2) - lines.at(i)(0) , lines.at(i)(3) - lines.at(i)(1) );
    theta = atan2( vp1.y , vp1.x  ) * 180.0 / CV_PI;


    
    if(theta<0)
      theta = 360.00 + theta;
    
    // Truncated histogram to get the cumulative value of the number of angles
    // contained in the angle range [int(theta) ; int(theta)+1]
    // AnglesCumulativesInts will then contain for each 0<i<360, the number of
    // angles occurrence anglesCumulativesInts[i] where 0 <
    // anglesCumulativesInts[i] < inf representing the occurrences of double
    // typed angles included in the range of real numbers [i ; i+1[. This will
    // be helpful to find the range of the angles with the most appearance.
    // Example if we loop thought the next succession of angles; 250.2 250.4
    // 250.1 250.9 ... The common truncated value of the succession is 250
    // which will imply that anglesCumulativesInts[250] == 4 is true.
    anglesCumulativesInts[static_cast<int>(theta)]++;

    
    // We truncate the obtained angle to conserve certains desire (decimalAnglesResolution) digits after the decimal point.
    thetaInt = static_cast<int>(theta * decimalAnglesResolution);
    angles.push_back(thetaInt);

    
  }

  cout<<"anglesCumulativesInts"<<endl;
  for(int i = 0 ; i < 361 ; i++)
  {
    if(anglesCumulativesInts[i]>0)
      cout<<i<<" : " <<anglesCumulativesInts[i]<<endl;
  }

  // Histogram where each angle value form a bin which is incremented in function of how many times this value appear in the angles vector 
  map<int , unsigned int> anglesHisto;
  map<int , unsigned int>::iterator anglesHistoIt ;

  int angleKey = 0;
  for(unsigned int i=0 ; i < angles.size() ; i++)
  {
    // Each angle becomes a key.
    angleKey = angles.at(i);
    // Looking if index exist already
    anglesHistoIt=anglesHisto.find(angleKey);

    // If it does not exits, create it and set its cumulative associated value to 1.
    if(anglesHistoIt == anglesHisto.end())
      anglesHisto.insert(pair<int , unsigned int> (angleKey , 1));
    // If it does exists, set its cumulative value to : old cumulative value + 1
    else
    {
      anglesHisto[angleKey] = anglesHisto[angleKey] + 1;
      
    }
  }

  cout<<"anglesHisto :"<<endl;
  for(anglesHistoIt = anglesHisto.begin(); anglesHistoIt != anglesHisto.end(); ++anglesHistoIt)
  {
    cout<< anglesHistoIt->first<<":"<<anglesHistoIt->second<<endl;;
  }
  cout<<endl;

  // We loop through all values of the histogram of angles values to create 
  // a second histogram containing all truncated (to int) anglesHisto's double indexes values . Each truncated index will be associated with the number of 
  // times this truncated value appears in anglesHisto. 
  
  
  anglesCumulativesInts[0] += anglesCumulativesInts[360];
  anglesCumulativesInts[360]= 0;

  set <unsigned int> indexesOfAnglesWithMaxCumulativeFound;


  unsigned int anglesMaxCumulativeValue;
  unsigned int anglesMaxCumulativeIndex = 0;
  unsigned int ordIndexesOfAnglesWithMaxCumulativeFound[NB_OF_ANGLE_CHOICES]={};
  bool angleFound;

  // Finding the range of the angles with the most appearance.
  for(unsigned int h = 0 ; h < NB_OF_ANGLE_CHOICES ; h++)
  {
    angleFound = false;
    anglesMaxCumulativeValue = 0;

    for(unsigned int i= 0 ; i < 360 ; i++)
    {

      if(anglesCumulativesInts[i]> anglesMaxCumulativeValue && indexesOfAnglesWithMaxCumulativeFound.find(i) == indexesOfAnglesWithMaxCumulativeFound.end() )
      {
        angleFound=true;
        anglesMaxCumulativeValue = anglesCumulativesInts[i];
        anglesMaxCumulativeIndex = i;

      }
    }
    if(angleFound)
    {
      indexesOfAnglesWithMaxCumulativeFound.insert(anglesMaxCumulativeIndex);
      ordIndexesOfAnglesWithMaxCumulativeFound[h] = anglesMaxCumulativeIndex;
    }
    else
    {
      break;
    }
  }


  // With the range found, we then loop through each value to calculate the 
  // average of this range with anglesHisto histogram.
  // Explained with example:
  // If 250 is contained in indexesOfAnglesWithMaxCumulativeFound
  // we loop from 250 to 250.9 incrementing by 0.1 requesting for each value, 
  // the occurrence in anglesHisto (double angle indexed histogram with  a .0 // precision) and then we calculate the average.
  unsigned int nbOfAnglesMax= 0;
  double sumOfAngles=0.0;
  vector <double> anglesOfRotation;
  for(unsigned int h = 0 ; h < indexesOfAnglesWithMaxCumulativeFound.size() ; h++)
  {
    
    nbOfAnglesMax = 0;
    sumOfAngles   = 0;
    angleKey = ordIndexesOfAnglesWithMaxCumulativeFound[h] * decimalAnglesResolution;
    for(int i = angleKey  ; i <= angleKey + decimalAnglesResolution ; i++)
    {
      
      anglesHistoIt = anglesHisto.find(i);
      if(anglesHistoIt != anglesHisto.end())
      {
        nbOfAnglesMax += anglesHistoIt->second;
        sumOfAngles += ((double) anglesHistoIt->first/decimalAnglesResolution) *              (double) anglesHistoIt->second;
      }
      
    }
   
    if(nbOfAnglesMax > 0)
      anglesOfRotation.push_back(static_cast<double> (sumOfAngles / nbOfAnglesMax));
    else
    {
      cout<<"RegionOfInterestOrientation::SetRegionOfInterestRotation(): warning no angle found for for angleKey : "<<angleKey<<endl;
    }
    
  }

  for(unsigned int i = 0 ; i < anglesOfRotation.size() ; i++)
  {
    cout<<"anglesOfRotation"<<i<<" : "<<anglesOfRotation.at(i)<<endl;
  }


  DummyTrackbar::createDummyTrackbar();
  
  String butName , selectionId = "selectedROI : " + to_string(this->getParcelOrientationAnalysisId()) ;

// Create a succession of propositions available as buttons for the user.
  for(unsigned int i = 0 ; i < anglesOfRotation.size() && i < NB_OF_ANGLE_CHOICES ; i++)
  {


    regionOfInterestOrientationCallbackParams[i].angleOfRotation = anglesOfRotation.at(i);
    regionOfInterestOrientationCallbackParams[i].regionOfInterestOrientation = this;
    butName =selectionId +" : "+ to_string(anglesOfRotation.at(i)) + "°";
    cvCreateButton(butName.c_str() , rotateButtons, (void*) &this->regionOfInterestOrientationCallbackParams[i], CV_PUSH_BUTTON, 0);
    
  }
// Creates button to confirm the selected rotation angle chosen by the user.
  butName = selectionId + " | Orientation Ok";
  cvCreateButton( butName.c_str(), okrotationButton , (void*) this , CV_PUSH_BUTTON, 0);

  //DEBUG PURPOSE ONLY
    
    rotateButtons( 0 , (void*) &this->regionOfInterestOrientationCallbackParams[1]);
    okrotationButton(0 , (void*) this);    
  //END DEBUG PURPOSE ONLY

}

/**
 * @brief      Callback function of the angle confirmation button
 *             "selection x : ok".
 * @details    Once the rotation angle is chosen, this function will start the
 *             next procedure represented by the execution of the
 *             regionOfInterestOrientation (in function) created instance.
 *
 * @param[in]  <unnamed>                 0
 * @param      regionOfInterestOrientationPtr  A void* pointer pointing to a
 *                                       RegionOfInterestOrientation instance.
 */
void okrotationButton(int , void * regionOfInterestOrientationPtr)
{

  RegionOfInterestOrientation *regionOfInterestOrientation = static_cast<RegionOfInterestOrientation *> (regionOfInterestOrientationPtr);
  
  if(!regionOfInterestOrientation)
    return;



  // Check if there is already a state counting the borders in current selected parcel
  if(regionOfInterestOrientation->parcelCounterId != 0)
  {
      cout<<"RegionOfInterestOrientation okrotationButton():  reusing parcel counter state"<<endl;

      // If true, the old state is re executed with a new given image.

      ParcelCounting * parcelCounting = (ParcelCounting*) State::get(regionOfInterestOrientation->parcelCounterId);

      shared_ptr<ImageFF> imageForParcelCounting = make_shared <ImageFF>(regionOfInterestOrientation->
          imageToOrientate->getImageMat().clone());
      
      imageForParcelCounting->setFilteredImageMat(
          regionOfInterestOrientation->
            imageToOrientate->getFilteredImageMat().clone());

      shared_ptr <CannyFilter> cannyFilter = make_shared<CannyFilter>();
      
      imageForParcelCounting->addFilter(cannyFilter);
    
      parcelCounting->setOrigImage(imageForParcelCounting);
      
      State::execState(regionOfInterestOrientation->parcelCounterId);

      
  }
  else
  {
      cout<<"RegionOfInterestOrientation okrotationButton():  creating new parcel counter state"<<endl;

      
      // If false, a new state is created and executed.
      unique_ptr<ParcelCounting> parcelCounting = make_unique<ParcelCounting>(regionOfInterestOrientation->getImageInfoExtractor() , regionOfInterestOrientation->imageToOrientate);
        
      

      
      string winNameOfParcelCounter="Parcel counter "+ to_string(regionOfInterestOrientation->getParcelOrientationAnalysisId());
      
      parcelCounting->winNameOfImageToAnalyse = winNameOfParcelCounter  ;
      
      
      shared_ptr<ImageFF> imageForParcelCounting = make_shared <ImageFF>(regionOfInterestOrientation->
          imageToOrientate->getImageMat().clone());
      
      
      imageForParcelCounting->setFilteredImageMat(
          regionOfInterestOrientation->
            imageToOrientate->getFilteredImageMat().clone());

      
      shared_ptr <CannyFilter> cannyFilter = make_shared<CannyFilter>();
      
      imageForParcelCounting->addFilter(cannyFilter);
      
      parcelCounting->setOrigImage(imageForParcelCounting);
      
      
      unsigned int parcelCounterId = State::registerState(move(parcelCounting));

      regionOfInterestOrientation->parcelCounterId=parcelCounterId;
      
      State::execState(parcelCounterId);

      
  }   
}



/**
 * @brief      Execute a rotation selected by the user.
 *
 * @param[in]  <unnamed>  0
 * @param      userdata   The userdata is a void* pointer pointing to a
 *                        RegionOfInterestOrientationButtonCallbackParams instance.
 */
void rotateButtons(int , void * userdata )
{
  RegionOfInterestOrientationButtonCallbackParams* mPOBCP = static_cast<RegionOfInterestOrientationButtonCallbackParams*>(userdata);
  // convert userdata to the right type
  if(!mPOBCP)
  {
    cout<<"RegionOfInterestOrientation rotateButtons() : null parameter"<<endl;
    return;

  }
  Mat imageToOrientateOrigMat = mPOBCP->regionOfInterestOrientation->imageToOrientateOrig->getImageMat();
  
  Mat imageToOrientateFilteredOrigMat = mPOBCP->regionOfInterestOrientation->imageToOrientateOrig->getFilteredImageMat();
  
  shared_ptr <ImageFF> imageToOrientate= mPOBCP->regionOfInterestOrientation->imageToOrientate;
  
  shared_ptr <ImageFF> imageToShow = mPOBCP->regionOfInterestOrientation->imageToShow;
  

  imageToOrientate->setImageMat(imageToOrientateOrigMat.clone());
  
  imageToOrientate->setFilteredImageMat(imageToOrientateFilteredOrigMat.clone());
  
  imageToOrientate->rotate(mPOBCP->angleOfRotation);
  
  Mat mat = imageToOrientate->getImageMat().clone();
  
  imageToShow->setImageMat(mat);
  
  imageToShow->drawGrid();

  


  namedWindow(mPOBCP->regionOfInterestOrientation->winName , WINDOW_NORMAL);
  resizeWindow(mPOBCP->regionOfInterestOrientation->winName , 600 , 600);
  imshow(mPOBCP->regionOfInterestOrientation->winName , imageToShow->getImageMat());

}


/**
 * @brief      Sets the image to orientate.
 *
 * @param[in]  imageToOrientate  The image to orientate
 */
void RegionOfInterestOrientation::setImageToOrientate(shared_ptr <ImageFF> imageToOrientate)
{
  this->imageToOrientateOrig = imageToOrientate;
  this->imageToOrientate = make_shared<ImageFF>(imageToOrientate->getImageMat().clone());
  this->imageToOrientate->setFilteredImageMat(this->imageToOrientateOrig->getFilteredImageMat().clone());
}



/**
 * @brief      Sets the parcel orientation analysis identifier. This id will
 *             allow to differentiate each region of interest from the first and
 *             main image to be rotated. Since multiples zone of the main image
 *             can be extracted in the SelectingRegionOfInterest state procedure
 *             and analyzed, this id will differentiate each one of them.
 *
 * @param[in]  id    The identifier
 */
void RegionOfInterestOrientation::setParcelOrientationAnalysisId(int id)
{
  if(this->parcelOrientationAnalysisId < 0 && id >= 0 )
  {
    this->parcelOrientationAnalysisId =  id;
  }
  else
  {
    cout<<"RegionOfInterestOrientation::setParcelOrientationAnalysisId(): parcel orientation analysis id not set"<<endl;
  }
}


/**
 * @brief      Gets the parcel orientation analysis identifier.
 *
 * @return     The parcel orientation analysis identifier.
 */
int RegionOfInterestOrientation::getParcelOrientationAnalysisId()
{
  return this->parcelOrientationAnalysisId;
}
