#include "Zone.hpp"

/**
 * @brief      Constructs the object.
 *
 * @param[in]  origImageFF      The color non filtered original image
 * @param[in]  drawableImageFF  The gray canny filtered drawable image
 * @param[in]  x                The x-axis upper left corner origin coordinate
 * @param[in]  y                The y-axis upper left corner origin coordinate
 * @param[in]  width            The width of the zone
 * @param[in]  height           The height of the zone
 * @param[in]  grayOriginalImage   The gray canny filtered original image
 * @param[in]  colorDrawableImage  The color non filtered drawable image
 */
Zone::Zone(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF ,  int x , int y , int width , int height) : super(origImageFF ,drawableImageFF)
{
	Mat origFilteredImageMat = origImageFF->getFilteredImageMat();
	
	// Verification of zone properties. If the origin coordinates of the zone isn't contained in the associated image, they will be defined to correspond the image origin (0,0).
	if(x < origFilteredImageMat.cols && x >=0)
		this->zone.x = x;
	else
	{
		this->zone.x = 0;
		cout<<"Zone::Zone(): Warning x not defined"<<endl;
	}	

	if(y < origFilteredImageMat.rows && y >= 0)
		this->zone.y = y;
	else
	{
		cout<<"Zone::Zone(): Warning y not defined"<<endl;
		cout<<"Zone::Zone(): y = "<<y<<endl;
		this->zone.y = 0;
	}	
	
	// Verification of zone properties. If the zone parameters go outside of the attached image border, they will be resized the stay inside the image borders.
	if(x + width > origFilteredImageMat.cols)
	{
		this->zone.width = width - (x + width - origFilteredImageMat.cols); 
	}
	else
	{
		this->zone.width = width;
	}

	if(y + height > origFilteredImageMat.rows)
	{
		this->zone.height = height - (y + height - origFilteredImageMat.rows); 
	}
	else
	{
		this->zone.height = height;
	}
	

}


/**
 * @brief      Returns a string representation of the object.
 *
 * @return     String representation of the object.
 */
string Zone::toString()
{
	return to_string(this->getX()) + "-" + to_string(this->getY()) + "-" + to_string(this->getW()) + "-" + to_string(this->getH());
}


/**
 * @brief      Find columns in zone containing the maximum quantity of border
 *             pixels. This is intended for a zone placed on a Canny filtered
 *             image.
 */
void Zone::findColWithMaxPixelsQty()
{
	// TODO : reduce the complexity, replace sort by linear search for max value in array
	if(!this->origImageFF){cout<<"Zone::findColWithMaxPixels() :no origImageFF avaible"<<endl; return ;}

	Mat origFilteredImageMat = origImageFF->getFilteredImageMat();
	
	vector<int> ordNbOfVertPixelsInZone;
	ordNbOfVertPixelsInZone.resize(zone.width);

	
	int relativej;
	for(int i = zone.y ; i < zone.y + zone.height ; i++ )
	{
		uchar * p = origFilteredImageMat.ptr<uchar>(i);

		for(int j = zone.x ; j < zone.x + zone.width ; j++)
		{
			relativej = j - zone.x;
			if(p[j] > 0)
				ordNbOfVertPixelsInZone.at(relativej)++;
		}
	}	
 
	int maxNbOfPixels =0, maxNbOfPixelsIndex;

 	for(unsigned int i = 0 ; i < ordNbOfVertPixelsInZone.size() ; i++)
 	{
 		if(ordNbOfVertPixelsInZone.at(i) > maxNbOfPixels)
 		{
 			maxNbOfPixels = ordNbOfVertPixelsInZone.at(i);
 			maxNbOfPixelsIndex = i;
 		}
 	}
	this->colIndexWithMaxPixels = maxNbOfPixelsIndex;
	this->colWithMaxPixelsQty = maxNbOfPixels;
}





/**
 * @brief      Gets the columns with maximum pixels qty. This is intended for a
 *             zone placed on a Canny filtered image.
 *
 * @return     The col with maximum pixels qty.
 */
int Zone::getColWithMaxPixelsQty()
{
	if(colWithMaxPixelsQty == -1)
	{
		this->findColWithMaxPixelsQty();
		return this->colWithMaxPixelsQty;
	}

	else
		return this->colWithMaxPixelsQty;


}

/**
 * @brief      Gets the column index with the maximum pixels quantity. This is
 *             intended for a zone placed on a Canny filtered image.
 *
 * @return     The column index in this zone with maximum pixels quantity.
 */
int Zone::getColIndexWithMaxPixels()
{
	if(colIndexWithMaxPixels == -1)
	{
		this->findColWithMaxPixelsQty();
		return this->colIndexWithMaxPixels;
	}
	else
		return this->colIndexWithMaxPixels;
}


/**
 * @brief      Gets the number of pixels contained in zone. This is intended for
 *             a zone placed on a Canny filtered image.
 *
 * @return     The number of pixels in the zone.
 */
int Zone::getNbOfPixels()
{
	if(this->nbOfPixels== -1)
	{
		Mat origFilteredImageMat = origImageFF->getFilteredImageMat();
		uchar * p ;
		this->nbOfPixels = 0;
		for(int i = this->zone.y ; i< this->zone.y + this->zone.height ; i ++ )
		{
			p = origFilteredImageMat.ptr <uchar> (i);
			for(int j = this->zone.x ; j < this->zone.x + this->zone.width ; j++ )
			{
				if(p[j] > 0)
				{
					this->nbOfPixels++;
				} 
			}
		}
	}
	
	return this->nbOfPixels;
	
}


/**
 * @brief      < Operator default definition.
 * @details    The number of pixels contained in the compared zones are used for
 *             making them orderable by default. This is intended for zones
 *             placed on a Canny filtered image.
 *
 * @param      otherZone  The other zone compared with this
 *
 * @return     True if this zone < otherZone in terms of pixel quantity
 *             contained in each zone, false otherwise
 */
bool Zone::operator<( Zone & otherZone)
{
	return this->getNbOfPixels() < otherZone.getNbOfPixels(); 
}

/**
 * @brief      == Operator default definition.
 * @details    The number of pixels contained in the compared zones are used for
 *             making them orderable by default. This is intended for zones
 *             placed on a Canny filtered image.
 *
 * @param      otherZone  The other zone compared with this zone
 *
 * @return     True if this zone == otherZone in terms of pixel quantity
 *             contained in each zone, false otherwise
 */bool Zone::operator == (Zone &otherZone)
{
	return this->getX() == otherZone.getX() && this->getY() == otherZone.getY()
	&& this->getW() ==  otherZone.getW() && this->getH() == otherZone.getH();
}


/**
 * @brief      != Operator default definition.
 * @details    The number of pixels contained in the compared zones are used for
 *             making them orderable by default. This is intended for zones
 *             placed on a Canny filtered image.
 *
 * @param      otherZone  The other zone compared with this zone
 *
 * @return     True if this zone == otherZone in terms of pixel quantity
 *             contained in each zone, false otherwise
 */
bool Zone::operator != (Zone &otherZone)
{
	return !(*this == otherZone);
}



/**
 * @brief      Draws this zone in colorDrawableImage the associated color image.
 *
 * @param[in]  r     red value in between 0 and 255
 * @param[in]  g     green value in between 0 and 255
 * @param[in]  b     blue value in between 0 and 255
 */
void Zone::drawC(int r , int g , int b)
{
	Mat drawableColorImageMat = this->drawableImageFF->getImageMat();
	rectangle(drawableColorImageMat , zone , Scalar(b , g , r) , 0);	
}


/**
 * @brief      Draws this zone in filtered associated image.
 */
void Zone::drawF()
{
	Mat filteredImageMat = this->drawableImageFF->getFilteredImageMat();
	rectangle(filteredImageMat , zone , Scalar(255,255,255));
}

/**
 * @brief      Draws a column in the selected color representing the vertical
 *             line in this zone where the maximum number of pixel has been
 *             found.This is intended for zones placed on a Canny filtered
 *             image.
 *
 * @param[in]  r     red value in between 0 and 255
 * @param[in]  g     green value in between 0 and 255
 * @param[in]  b     blue value in between 0 and 255
 */
void Zone::drawColWithMaxPixelQtyC(int r , int g , int b)
{
	Mat drawableColorImageMat = this->drawableImageFF->getImageMat();
	Point p1(zone.x + this->getColIndexWithMaxPixels() , zone.y);
	Point p2(p1.x , zone.y + zone.height);

	
	line(drawableColorImageMat , p1 , p2 , Scalar(b , g , r) );	
}


bool SortByNbOfPixels::operator()(shared_ptr<Zone>& zone1 , shared_ptr<Zone>& zone2 )
{
	return zone1->getNbOfPixels() > zone2->getNbOfPixels();
}	

bool SortByColWithMaxPixels::operator()(shared_ptr<Zone>& zone1 , shared_ptr<Zone>& zone2 )
{
	return zone1->getColWithMaxPixelsQty() > zone2->getColWithMaxPixelsQty();
}

bool SortByCoords::operator()(shared_ptr<Zone>& zone1 , shared_ptr<Zone>& zone2 )
{
	if (zone1->getX() < zone2->getX())
		return true;
	return zone1->getY() < zone2->getY();
}

/**
 * @brief      { operator_description }
 *
 * @param      output  The output
 * @param      z       { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
ostream& operator<<( ostream &output, Zone &z ) 
{ 
	output <<" x:"<<z.zone.x
	    <<" y:"<<z.zone.y
	    <<" width:"<<z.zone.width
	    <<" heigth:"<<z.zone.height
	    <<" nbOfPixels:"<<z.getNbOfPixels()
	    <<endl;
	return output;            
} 


/**
 * @brief      Gets the x-axis upper left corner origin of this zone.
 *
 * @return     The x the x-axis upper left corner origin of this zone.
 */
int Zone::getX()
{
	return this->zone.x;
}


/**
 * @brief      Gets the y-axis upper left corner origin of this zone.
 *
 * @return     The y the y-axis upper left corner origin of this zone.
 */
int Zone::getY()
{
	return this->zone.y;
}

/**
 * @brief      Gets the width in pixels of this zone.
 *
 * @return     The width in pixels of this zone.
 */
int Zone::getW()
{
	return this->zone.width;
}
	
/**
 * @brief      Gets the height in pixels of this zone.
 *
 * @return     The height in pixels of this zone.
 */
int Zone::getH()
{
	return this->zone.height;
}


/**
 * @brief      Sets the (x,y) origin coordinates of the upper left corner, the width and the height of the zone.
 *
 * @param[in]  x       The x the x-axis upper left corner origin of this zone.
 * @param[in]  y       The y the y-axis upper left corner origin of this zone.
 * @param[in]  width   The width in pixels of this zone.
 * @param[in]  height  The height in pixels of this zone.
 */
void Zone::setXYWH(int x , int y , int width , int height) 
{	
	Mat origFilteredImageMat = this->origImageFF->getFilteredImageMat();

	if( x >= 0 &&  x < origFilteredImageMat.cols)
		this->zone.x = x;
	else 
		cout<<"Zone::setXYWH() : x not valid"<<endl;
	
	if( y >= 0 && y < origFilteredImageMat.rows)
		this->zone.y = y;
	else
		cout<<"Zone::setXYWH() : y not valid"<<endl;

	if(x + width < origFilteredImageMat.cols)
		this->zone.width = width;
	else
		cout<<"Zone::setXYWH() : width not valid"<<endl;

	if(y + height < origFilteredImageMat.rows)
		this->zone.height = height;
	else 
		cout<<"Zone::setXYWH() : height not valid"<<endl;
}




/**
 * @brief      Return the coordinates of the upper left corner of this zone as
 *             a string.
 *
 * @return     The coordinates of the upper left corner of this zone as
 *             a string.
 */
string Zone::coordsToString()
{
	return to_string(this->getX())+"-"+to_string(this->getY());
}



void Zone::drawHorInBetweenSurface(Zone &zone , int vertSpacing)
{

	Mat drawableColorImageMat = this->drawableImageFF->getImageMat();
	
	Point p1 , p2;

	if(p1.y <= p2.y)
	{
		p1.y = this->getY() + this->getH();
		p2.y = zone.getY();
	} 
	else
	{
		p2.y = zone.getY() + zone.getH();
		p1.y = this->getY();	
	}
	

	for(int j = 0 ; j < drawableColorImageMat.cols; j+= vertSpacing)
	{
	
		p1.x = j ;
		p2.x = j ;
		
		line(drawableColorImageMat , p1 , p2 , Scalar(0 , 255 , 0) , 1 );
	}
}