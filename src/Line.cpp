#include "Line.hpp"


Line::Line(){}

/**
 * @brief      Constructs the object.
 *
 * @param[in]  filteredOriginalImage   The gray original image
 * @param[in]  grayDrawableImage   The gray drawable image
 * @param[in]  colorOriginalImage  The color original image
 * @param[in]  colorDrawableImage  The color drawable image
 * @param[in]  x1                  The x-axis coordinate of the first Point of
 *                                 the wanted line
 * @param[in]  y1                  The y-axis coordinate of the first Point of
 *                                 the wanted line
 * @param[in]  x2                  The x-axis coordinate of the second Point of
 *                                 the wanted line
 * @param[in]  y2                  The y-axis coordinate of the second Point of
 *                                 the wanted line
 */
Line::Line(shared_ptr<ImageFF> origImageFF , shared_ptr<ImageFF> drawableImageFF , int x1 , int y1 , int x2 , int y2) : DrawableGeometStruct(origImageFF ,drawableImageFF)
{
	const Mat origFilteredImageMat = origImageFF->getFilteredImageMat();
	
	// Verification of line properties. If a point of the line isn't contained in the associated image, it will be set to (0,0).
	if(x1 < origFilteredImageMat.cols && x1 >=0 && y1 < origFilteredImageMat.rows && y1 >=0)
	{
		this->p1.x = x1;
		this->p1.y = y1;
	}
	else
	{
		p1.x = 0;
		p1.y = 0;
		cout<<"Line::Line(): Warning P1 not defined"<<endl;
	}	

	if(x2 < origFilteredImageMat.cols && x2 >= 0 && y2 < origFilteredImageMat.rows && y2 >= 0)
	{
		this->p2.x = x2;
		this->p2.y = y2;
	}	
	else
	{
		p2.x = 0;
		p2.y = 0;
		cout<<"Line::Line(): Warning P2 not defined"<<endl;
	}	
}

/**
 * @brief      Checks if two lines instance's are considered equal.
 *
 * @details    More precisely, checks if the coordinates of this line are equal to the otherLine coordinates
 * @param      otherLine  The other line to be compared
 *
 * @return     True if the compared line are equal, false otherwise.
 */
bool Line::operator == (Line &otherLine)
{
	return this->getX1() == otherLine.getX1() && this->getY1() == otherLine.getY1()
	&& this->getX2() ==  otherLine.getX2() && this->getY2() == otherLine.getY2();
}


/**
 * @brief      Checks if two lines instance's are considered not equal.
 *
 * @details    More precisely, checks if the coordinates of this line are not
 *             equal to the otherLine coordinates
 *
 * @param      otherLine  The other line to be compared
 *
 * @return     True if the compared line are not equal, false otherwise.
 */
bool Line::operator != (Line &otherLine)
{
	return !(*this == otherLine);
}

/**
 * @brief      Draws line in associated color image (colorDrawableImage)
 *
 * @param[in]  r     red value varying from 0 to 255
 * @param[in]  g     green value varying from 0 to 255
 * @param[in]  b     blue value varying from 0 to 255
 */
void Line::drawC(int r , int g , int b)
{
	Mat colorDrawableImageMat = this->drawableImageFF->getImageMat();
	line(colorDrawableImageMat , p1 , p2 , Scalar(b , g , r));	
}


/**
 * @brief      Draws line in associated filtered image
 */
void Line::drawF()
{
	Mat drawableFilteredImageMat = this->drawableImageFF->getFilteredImageMat();
	line(drawableFilteredImageMat , p1 , p2 , Scalar(255,255,255));
}


/**
 * @brief      Outputs in console useful information about this line
 *
 * @param      output  The output
 * @param      line    The line of the characteristics to be shown in
 *                     console
 *
 * @return     returns output stream
 */
ostream& operator<<( ostream &output, Line &line ) 
{ 
	output <<" x1:"<<line.p1.x
	    <<" y1:"<<line.p1.y
	    <<" x2:"<<line.p2.x
	    <<" y2:"<<line.p2.y<<endl;
	return output;            
} 


/** 
 *
 * @return     The x-axis value of the first point
 */
int Line::getX1()
{
	return this->p1.x;
}

	
/**
 *
 * @return     The y-axis value of the first point
 */
int Line::getY1()
{
	return this->p1.y;
}


/**
 *
 * @return     The x-axis value of the second point
 */
int Line::getX2()
{
	return this->p2.x;
}
	

/**
 *
 * @return     The y-axis value of the second point
 */
int Line::getY2()
{
	return this->p2.y;
}


/**
 *
 * @return     The euclidean distance between to the points defining this line
 */
double Line::eucledianDist()
{
	return sqrt(pow(p2.x - p1.x , 2) + pow(p2.y - p1.y , 2));
}
	


/**
 * @brief      Modifies the current line coordinates values
 *
 * @param[in]  x1    The x-axis value of the first point
 * @param[in]  y1    The y-axis value of the first point
 * @param[in]  x2    The x-axis value of the second point
 * @param[in]  y2    The y-axis value of the second point
 */
void Line::setp1p2coords(int x1 , int y1 , int x2 , int y2) 
{	
	const Mat origFilteredImageMat = this->origImageFF->getFilteredImageMat();

	// Verification of zone properties. If the origin coordinates of the zone isn't contained in the associated image, they will be defined to correspond the image origin (0,0).
	if(x1 < origFilteredImageMat.cols && x1 >=0 && y1 < origFilteredImageMat.rows && y1 >=0)
	{
		this->p1.x = x1;
		this->p1.y = y1;
	}
	else
	{
		p1.x = 0;
		p1.y = 0;
		cout<<"Line::Line(): Warning P1 not defined"<<endl;
	}	

	if(x2 < origFilteredImageMat.cols && x2 >= 0 && y2 < origFilteredImageMat.rows && y2 >= 0)
	{
		this->p2.x = x2;
		this->p2.y = y2;
	}	
	else
	{
		p2.x = 0;
		p2.y = 0;
		cout<<"Line::Line(): Warning P2 not defined"<<endl;
	}	
}




