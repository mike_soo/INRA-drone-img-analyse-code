#include "FilteringForOrientation.hpp"



FilteringForOrientation::FilteringForOrientation(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , unsigned int idSelectedRegionOfInterest , shared_ptr<Image> selectedRegionOfInterest) : super(  imageInfoExtractor , idSelectedRegionOfInterest , selectedRegionOfInterest)
{
	this->winNameOfImageToAnalyse = "Filtering to orientate | selectedROI : " + to_string(idSelectedRegionOfInterest);
    
}
	




void FilteringForOrientation::execBehavior()
{
	// Debug purpose only !

		filteringForOrientationOkButton(0 , (void*) this);

	// end Debug purpose only !
}


/**
 * @brief      Initialization of this instance.
 * @details    This initialization include the creation, association and
 *             application of the Canny filter to the selected region of
 *             interest.
 */
void FilteringForOrientation::init()
{
    
    
    shared_ptr<CannyFilter> cannyFilter = make_shared<CannyFilter>();
    this->selectedRegionOfInterest->addFilter(cannyFilter);
    
    for(int i = 0 ; i < 4 ; i++)
    {
        cannyFilter->borderPoints[i] = this->pointsOfRegionOfInterest[i];
    }

    namedWindow( this->winNameOfImageToAnalyse , WINDOW_NORMAL);
    resizeWindow(this->winNameOfImageToAnalyse , 600 , 600);
    imshow(this->winNameOfImageToAnalyse , this->selectedRegionOfInterest->getImageMat());
    
    createTrackbar( "Min canny threshold:", 
        this->winNameOfImageToAnalyse,
        &cannyFilter->lowThreshold  ,
        cannyFilter->max_lowThreshold,
        filtering, (void*) this );
    
	DummyTrackbar::createDummyTrackbar();

    filtering(0 , (void*)this);    
    string filteringForOrientationButtonName = "selected : " + 
    	to_string(this->getIdOfSelectedRegionOfInterest()) + " filtering Ok";
    cvCreateButton(filteringForOrientationButtonName.c_str() , filteringForOrientationOkButton, (void*) this, CV_PUSH_BUTTON, 0);

}


/**
 * @brief      Callback function invoked when the region of interest canny
 *             filtering is ready to be oriented horizontally.
 *
 * @param[in]  <unnamed>                   0
 * @param      filteringForOrientationPtr  A (void*) FilteringForOrientatin
 *                                         pointer pointer to a
 *                                         FilteringForOrientation instance.
 */
void filteringForOrientationOkButton(int , void * filteringForOrientationPtr)
{
    FilteringForOrientation *filteringForOrientation = static_cast<FilteringForOrientation *> (filteringForOrientationPtr);
  
  	if(!filteringForOrientation)
    	return;
    
    // Verification if there exist an associated image orientation procedure.
    if(filteringForOrientation->roiOrientationId == 0)
    {
        // Creation and execution of the orientation procedure.
        unique_ptr<RegionOfInterestOrientation> regionOfInterestOrientation = make_unique<RegionOfInterestOrientation>(filteringForOrientation->getImageInfoExtractor() );

        regionOfInterestOrientation->setImageToOrientate( filteringForOrientation->selectedRegionOfInterest);
        regionOfInterestOrientation->setParcelOrientationAnalysisId(filteringForOrientation->getIdOfSelectedRegionOfInterest());
	
        unsigned int roiOrientationId = State::registerState(move(regionOfInterestOrientation));
	    State::execState(roiOrientationId);

        filteringForOrientation->roiOrientationId = roiOrientationId; 
    }
    else
    {
    	State::execState(filteringForOrientation->roiOrientationId);
    }

}