#include "Filter.hpp"
#include "ImageFF.hpp"


/**
 * @brief      Gets the image.associated with this filter
 *
 * @return     The image that is associated with this filter.
 */
ImageFF* Filter::getImage()
{
	return this->image;
}

/**
 * @brief      Set the image that will be modified when applying this filter.
 *
 * @param[in]  image  The image that is associated with this filter.
 */
void Filter::setImage(ImageFF* image)
{
	if(image!= nullptr)
	{
		this->image = image;
	}	

	else
	{
		image= nullptr;
		cout<<"Filter::setImage(): this->image not set"<<endl;
	}
}


/**
 * @brief      Pre-processing method that ensures the availability of the image attribute associated with this filter before starting the derived class overridden filtering operations. 
 */
void Filter::applyFilter()
{
	if(image== nullptr)
	{
		cout<<"Filter::applyFilter(): image expired filter not applied "<<endl;
		return;
	}
  	this->applyFilter(image);
}