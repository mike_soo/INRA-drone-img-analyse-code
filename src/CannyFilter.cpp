#include "CannyFilter.hpp"

using namespace cv;

/**
 * @brief      Override function allows to apply canny filtering to an Image
 *             instance.
 * @details    Called by Filter::applyFilter, will modify the image attribute of
 *             the Filter class.
 *
 * @param[in]  image  A shared_ptr given by the base class Filter::applyFilter()
 *                    ensuring that the image associated with this filter stays
 *                    alive while applying modifications.
 */
void CannyFilter::applyFilter(ImageFF* image)
{	
	
	Mat src_gray;
	Mat detected_edge ;
	Mat imageMat      = this->image->getImageMat();
  	
  	
	/// Convert the image to grayscale
	cvtColor( imageMat, src_gray, COLOR_RGB2GRAY );
	

	blur( src_gray, detected_edge, Size(3,3) );
	
	/// Canny detector
	Canny( detected_edge , detected_edge , this->lowThreshold, this->max_lowThreshold, kernel_size );
	this->image->setFilteredImageMat(detected_edge);


	
  
	if( borderPoints[0].x == 0 && borderPoints[0].y == 0 &&
		borderPoints[1].x == 0 && borderPoints[1].y == 0 && 
		borderPoints[2].x == 0 && borderPoints[2].y == 0 &&
		borderPoints[3].x == 0 && borderPoints[3].y == 0)
 			return;
  	
  	
	for(int i = 0 ; i < 4 ; i++)
	{
		this->image->removeLineInFilteredImg(borderPoints[i] , borderPoints[(i+1)%4]);
	}
	

}


void CannyFilter::setImage(ImageFF* image)
{
	super::setImage(image);
	for(int i = 0 ; i < 4 ; i++)
	{	
		borderPoints[i].x=0;
		borderPoints[i].y=0;
	}
}


void CannyFilter::lookForBorderPoints()
{
	Mat imageToFilterMat = this->image->getImageMat();
	Vec3b * p;

	Vec3b red(0 , 0 , 255);

	// Border poitns counter.
	int bCpt = 0;

	for(int i =0 ; i < 4 ; i++)
	{
		borderPoints[i] = Point(0,0);
	}

	for(int i =0 ; i < imageToFilterMat.rows ; i ++)
	{
		p = imageToFilterMat.ptr<Vec3b> (i);		
		for(int j = 0 ; j < imageToFilterMat.cols ; j++)
		{

			if (p[j][2] >= 30 && p[j][1] == 0 && p[j][0] == 0)
			{
				cout<<"CannyFilter::lookForBorderPoints() : color found at ("<<j<<","<<i<<")"<<endl;
				borderPoints[bCpt++] = Point(j , i); 

				if(bCpt == 4)
				{
					Point temp = borderPoints[2];
					borderPoints[2] = borderPoints[3];
					borderPoints[3] = temp;
					return;
				}

			}	

		}
	}



	cout<<"CannyFilter::loofForBorderPoints() : border points not all found"<<endl<<"\t found :"<< bCpt <<" borders"<<endl;
		

		
}