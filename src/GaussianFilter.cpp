#include "GaussianFilter.hpp"


/**
 * @brief      Override function allows to apply gaussian filtering to an
 *             Image instance.
 *
 * @details    Called by Filter::applyFilter, will modify the image attribute of
 *             the Filter class.
 *
 * @param[in]  image  The image
 */
void GaussianFilter::applyFilter(ImageFF* image)
{
	GaussianBlur( image->getImageMat(), image->getFilteredMat(), Size(3,3), 0, 0, BORDER_DEFAULT );	
}