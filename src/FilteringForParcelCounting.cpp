#include "FilteringForParcelCounting.hpp"



FilteringForParcelCounting::FilteringForParcelCounting(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , unsigned int idSelectedRegionOfInterest , shared_ptr<Image> selectedRegionOfInterest) : super(  imageInfoExtractor , idSelectedRegionOfInterest , selectedRegionOfInterest)
{
    this->winNameOfImageToAnalyse = "Parcel counting | selectedROI : " + to_string(idSelectedRegionOfInterest);
}
	




void FilteringForParcelCounting::execBehavior()
{

}


/**
 * @brief      Initialization of this instance.
 * @details    This initialization include the creation, association and
 *             application of the Canny filter to the selected region of
 *             interest.
 */
void FilteringForParcelCounting::init()
{
    
    
    shared_ptr<CannyFilter> cannyFilter = make_shared<CannyFilter>();
    this->selectedRegionOfInterest->addFilter(cannyFilter);
    
    for(int i = 0 ; i < 4 ; i++)
    {
        cannyFilter->borderPoints[i] = this->pointsOfRegionOfInterest[i];
    }

    namedWindow( this->winNameOfImageToAnalyse , WINDOW_NORMAL);
    resizeWindow(this->winNameOfImageToAnalyse , 600 , 600);
    imshow(this->winNameOfImageToAnalyse , this->selectedRegionOfInterest->getImageMat());
    
    createTrackbar( "Min canny threshold:", 
        this->winNameOfImageToAnalyse,
        &cannyFilter->lowThreshold  ,
        cannyFilter->max_lowThreshold,
        filtering, (void*) this );
    
    
    filtering(0 , (void*)this);    
    
    string filteringForParcelCoutingButtonName = "selected : " + 
    	to_string(this->getIdOfSelectedRegionOfInterest()) + " filtering Ok";
    cvCreateButton(filteringForParcelCoutingButtonName.c_str() , filteringForParcelCoutingOkButton, (void*) this, CV_PUSH_BUTTON, 0);

}


/**
 * @brief      Callback function invoked when the region of interest canny
 *             filtering is ready to be oriented horizontally.
 *
 * @param[in]  <unnamed>                   0
 * @param      filteringForParcelCoutingPtr  A (void*) FilteringForOrientatin
 *                                         pointer pointer to a
 *                                         FilteringForParcelCounting instance.
 */
void filteringForParcelCoutingOkButton(int , void * filteringForParcelCoutingPtr)
{
    FilteringForParcelCounting *filteringForParcelCouting = static_cast<FilteringForParcelCounting *> (filteringForParcelCoutingPtr);
  
  	if(!filteringForParcelCouting)
    	return;
    
    // Verification if there exist an associated image orientation procedure.
    if(filteringForParcelCouting->roiOrientationId == 0)
    {
        // Creation and execution of the orientation procedure.
        unique_ptr<ParcelCounting> parcelCounting = make_unique<ParcelCounting>(filteringForParcelCouting->getImageInfoExtractor() , filteringForParcelCouting->selectedRegionOfInterest );

        
        
        unsigned int roiParcelCounterId = State::registerState(move(parcelCounting));
	    State::execState(roiParcelCounterId);

        filteringForParcelCouting->roiParcelCounterId = roiParcelCounterId; 
    }
    else
    {
    	State::execState(filteringForParcelCouting->roiParcelCounterId);
    }

}