#include "SelectingRegionsOfInterest.hpp"


/**
 * @brief      Constructs the object.
 *
 * @param[in]  imageInfoExtractor  The image information extractor
 */
SelectingRegionsOfInterest::SelectingRegionsOfInterest(shared_ptr<ImageInfoExtractor>  imageInfoExtractor) : State(imageInfoExtractor)
{
    string stateName="SelectingRegionsOfInterest";
    this->setStateName(stateName);
}


/**
 * @brief      Saves each click made by the user for a region of interest
 *             selection.
 *
 * @param[in]  event      The event
 * @param[in]  x          x-axis coordinate of the click
 * @param[in]  y          y-axis coordinate of the click
 * @param[in]  <unnamed>  A void* pointer pointing to an
 *                        SelectingRegionsOfInterest instance.
 */
void onMouseSelectingRegionsOfInterest( int event, int x, int y, int, void* selectingRegionsOfInterestPtr)
{
    
	SelectingRegionsOfInterest *selectingRegionsOfInterest = (SelectingRegionsOfInterest*) selectingRegionsOfInterestPtr ;

	
	if(!selectingRegionsOfInterest)
		return;
    
    switch( event )
    {
    case EVENT_LBUTTONDOWN:
        {
            
        }
        break;
    case EVENT_LBUTTONUP:
	    {
	    	Mat colorImageMatWithROIs = selectingRegionsOfInterest->colorImageWithROIs->getImageMat();
	    	Point pUser(x , y);
	    	cout<<"\t puser:"<<pUser<<endl;
    		selectingRegionsOfInterest->pointsOfRegionOfInterest.push_back(pUser);

    		Scalar colorOfPointsOfInterest(102 , 0 , 204);
	    	selectingRegionsOfInterest->colorImageWithROIs->drawPointOfInterest(pUser , colorOfPointsOfInterest);
	    	imshow("Selecting regions of interest" , colorImageMatWithROIs);
	    	
	    	// If we have 4 defined point we define a region of interest and crop the main image to analyze this new defined region.
		    if(selectingRegionsOfInterest->pointsOfRegionOfInterest.size() == 4)
		    {

		    	
				Mat src= selectingRegionsOfInterest->colorImageOrig->getImageMat();
				Mat black(src.rows, src.cols, src.type(), cv::Scalar::all(0));
				Mat dst(src.rows, src.cols, src.type(), cv::Scalar::all(0));
				Mat mask(src.rows, src.cols, CV_8UC3, cv::Scalar(0 , 0 , 0));
				vector< vector<Point> >  co_ordinates;
				co_ordinates.push_back(selectingRegionsOfInterest->pointsOfRegionOfInterest);
				
				// Creates mask where the polygon created by 4 user clicks will be stored.
				drawContours( mask,co_ordinates,0, Scalar(255 , 255 , 255),CV_FILLED, 8 );

				// Draws the unfilled polygon created by the user's last 4 
				// clicks in the colored image.
				drawContours( selectingRegionsOfInterest->colorImageWithROIs->getImageMat(),co_ordinates,0, Scalar(0 , 0 , 255), 4 , 8 );
				
				imshow("Selecting regions of interest" , colorImageMatWithROIs);

				
				// Extraction of the polygon zone in the colored main Image that will be referred as a the region of interest
				bitwise_and(mask , src , dst);

				// Setting image (region of interest) to orientate.
				shared_ptr<Image> imageROI = make_shared<Image> (dst);
				
				
				// Creation of next active process state.
				unique_ptr<FilteringForOrientation> filteringForOrientation = make_unique<FilteringForOrientation>(selectingRegionsOfInterest->getImageInfoExtractor() , selectingRegionsOfInterest->getNewIdForSelectedRoi(), imageROI);
				
				for(int i = 0 ; i < 4 ; i++)
				{
					filteringForOrientation->pointsOfRegionOfInterest[i]= selectingRegionsOfInterest->pointsOfRegionOfInterest.at(i);


					imageROI->drawPoint(selectingRegionsOfInterest->pointsOfRegionOfInterest.at(i) , Vec3b(0 , 0 , 255));
				}
				selectingRegionsOfInterest->pointsOfRegionOfInterest.clear();
				
			    // Starting the next state.
			    unsigned int filteringForOrientationStateId = State::registerState(move(filteringForOrientation));
			    State::execState(filteringForOrientationStateId);

			    
			    
		    }
	    }
    	break;
    }
}


/**
 * @brief      Setting image display and mouse callback function.
 */
void SelectingRegionsOfInterest::init()
{
	string winName = "Selecting regions of interest";
	namedWindow(winName , WINDOW_NORMAL);
	resizeWindow(winName , 600 ,600);
	imshow(winName , this->colorImageWithROIs->getImageMat());
	setMouseCallback(winName, onMouseSelectingRegionsOfInterest , (void*)this);
}

//
// @brief      Behavior of state, since it's all managed by event listener, this
//             section remains empty.
//
void SelectingRegionsOfInterest::execBehavior()
{

	
	
	

	onMouseSelectingRegionsOfInterest( EVENT_LBUTTONUP, 519, 4700 , 0, (void*)this);
	onMouseSelectingRegionsOfInterest( EVENT_LBUTTONUP, 3884, 1004 , 0, (void*)this);
	onMouseSelectingRegionsOfInterest( EVENT_LBUTTONUP, 4439, 1460 , 0, (void*)this);
	onMouseSelectingRegionsOfInterest( EVENT_LBUTTONUP, 1044, 5196 , 0, (void*)this);

}

/**
 * @brief      Sets the color image where regions of interest going to be
 *             extracted..
 *
 * @param[in]  colorImage  The color image
 */
void SelectingRegionsOfInterest::setColorImage(shared_ptr <Image> colorImage)
{
	this->colorImageOrig = colorImage;
	this->colorImageWithROIs = make_shared<Image>(colorImage->getImageMat().clone());
}

/**
 * @brief      Getter of ids for new selected regions.
 *
 * @return     The new identifier for a selected region of interest (ROI).
 */
int SelectingRegionsOfInterest::getNewIdForSelectedRoi()
{
	return this->numberOfSelectedROIs++;
}