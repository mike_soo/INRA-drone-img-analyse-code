#include "ImageFF.hpp"

ImageFF::ImageFF()
{
	
}

/**
 * @brief      Constructs the object.
 *
 * @param[in]  mat   The matrix representing an image from opencv.
 */
ImageFF::ImageFF(Mat imageMat) : Image(imageMat)
{

}

ImageFF::~ImageFF()
{
	
}
/**
 * @brief      Sets the image matrix.
 *
 * @param[in]  mat   The image matrix
 */
void ImageFF::setFilteredImageMat(Mat filteredImageMat)
{
	this->filteredImageMat = filteredImageMat;
}



/**
 * @brief      Adds a filter to the vector of filters used by this Image.
 *
 * @param[in]  filter  The filter
 */
void ImageFF::addFilter(shared_ptr<Filter> filter)
{
	if(filter)
	{
		this->filters.push_back(filter);
		filter->setImage(this);
	}	
}



/**
 * @brief      Applies all added filters successively in the added order of
 *             each filter.
 */
void ImageFF::applyFilters()
{
	for(unsigned int i = 0 ; i < this->filters.size();  i ++)
	{

		this->filters.at(i)->applyFilter();
	}
}




/**
 * @brief      Apply the i-th filter contained in the vector of filters. 
 *
 * @param[in]  i     { parameter_description }
 */
void ImageFF::applyFilter(unsigned int i)
{
	if(i < this->filters.size() && i >= 0)
	{
		this->filters.at(i)->applyFilter();
	}
}


/**
 * @brief      Rotate the current filtered image mat and the normal image mat
 *             of angleOfRotation degrees using the center of the image as the
 *             center of rotation.
 *
 * @param[in]  angleOfRotation  The angle of rotation in degrees.
 */
void ImageFF::rotate(double angleOfRotation)
{
	
	super::rotate(angleOfRotation);
	super::rotate(angleOfRotation , this->filteredImageMat);

	
}


/**
 * @brief      Gets the matrix mat of the filtered image.
 *
 * @return     The filtered matrix.
 */
Mat ImageFF::getFilteredImageMat() 
{
	return this->filteredImageMat;
}



/**
 * @brief      Removes all the pixels in the filterd  image in between 2 point
 *             in a connectivity-connected line.
 *
 * @param[in]  p1            The first point
 * @param[in]  p2            The second point
 * @param[in]  connectivity  The connectivity of the existing line created
 *                           between this 2 points.
 * @param[in]  tickness      The tickness
 */
void ImageFF::removeLineInFilteredImg(Point &p1 , Point &p2 , int connectivity , int tickness )
{
	if(connectivity != 4 && connectivity != 8 )
	{
		cout<<"removeLineInGrayImg(): connectivity must be equal to 4 or 8"<<endl;
		return;
	}

	if(tickness <= 0 || tickness % 2 != 1 )
	{
		cout<<"removeLineInGrayImg(): tickness must be even and superior to 0"<<endl;
		return;
	}

	Mat imageMat = this->getFilteredImageMat();
	cv::LineIterator it(imageMat, p1, p2, 8);
	vector<Point> points(it.count);

	int sideTickness = (double) tickness / 2.0;

	for(int i = 0; i < it.count; i++, ++it)
	{
	    points[i] = it.pos();
	}

	uchar * p = nullptr;
	Point cp;  // For current point
	for(unsigned int i = 0 ; i < points.size() ; i ++)
	{
		cp = points.at(i);

		for(int xtickness = cp.x - sideTickness; xtickness <= cp.x + sideTickness  ; xtickness++)
		{
			if(xtickness >= 0 && xtickness < imageMat.cols)
			{
				for(int ytickness = cp.y - sideTickness; ytickness <= cp.y + sideTickness ; ytickness++)
				{
					if(ytickness >= 0 && ytickness < imageMat.rows)
					{
						p = imageMat.ptr<uchar>(ytickness);
						p[xtickness] = 0;
					}
				}
			}
		}
	}
	

}

