#include "ImageInfoExtractor.hpp"
 




/**
 * @brief      Sets the main original image which will be accessible to all the
 *             available states.
 *
 * @param      image  The image
 */
void ImageInfoExtractor::setMainImage(Image * image)
{
	this->image = shared_ptr<Image>(image);
	this->originalImage  = make_shared<Image> (image->getImageMat().clone());
}
 
/**
 * @brief      Sets the main  image which will be accessible to all the
 *             available states.
 *
 * @param[in]  image  The main image
 */
void ImageInfoExtractor::setMainImage(shared_ptr <Image> image)
{
	this->image = image;
	this->originalImage  = make_shared<Image> (image->getImageMat().clone());
}

/**
 * @brief      Gets the original non modifiable image.
 *
 * @return     The original non modifiable image.
 */
shared_ptr<const Image> ImageInfoExtractor::getOriginalImage()
{
	return this->originalImage;
}


/**
 * @brief      Gets the main image.
 *
 * @return     The main image.
 */
shared_ptr<Image> ImageInfoExtractor::getMainImage()
{
	return this->image;
}
