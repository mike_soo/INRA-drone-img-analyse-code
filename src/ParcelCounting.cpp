#include "ParcelCounting.hpp"




/**
 * @brief      Constructs the object.
 *
 * @param[in]  imageInfoExtractor  The image information extractor
 * @param[in]  imageFF             The image containing a canny filtered image
 *                                 and its original colored version
 */
ParcelCounting::ParcelCounting(shared_ptr<ImageInfoExtractor>  imageInfoExtractor , shared_ptr<ImageFF> imageFF) : State(imageInfoExtractor)
{
	this->origImageFF = imageFF;	
	
  	this->drawableImageFF = make_shared<ImageFF>(this->origImageFF->getImageMat().clone());

  	
  	this->drawableImageFF->setFilteredImageMat(this->origImageFF->getFilteredImageMat().clone());

  	
	modelLineHorDistParcel.setImages(this->origImageFF , this->drawableImageFF);

	
	modelLineVerDistParcel.setImages(this->origImageFF , this->drawableImageFF);
}


/**
 * @brief      Starts the region's of interest horizontal analysis where
 *             horizontal crops borders are deduced.
 *
 * @param[in]  horizontalBorderError  The horizontal border error. In other
 *                                    words, this parameter fixes the height of
 *                                    each explored horizontal zone. The width
 *                                    will be equal the the image number of
 *                                    colons.
 */
void ParcelCounting::horizontalCounting(  int horizontalBorderError)
{	
	
	
	// Switching to horizontal counting mode.
	this->activateHorCounting();

	// Reseting old images and deductions.
	this->zone1ForHorDistDeduc = nullptr;
	this->zone2ForHorDistDeduc = nullptr;

	Mat drawableFilteredImageMat = this->drawableImageFF->getFilteredImageMat();
	Mat origFilteredImageMat = this->origImageFF->getFilteredImageMat();
	
	origFilteredImageMat.copyTo(drawableFilteredImageMat);
	
	Mat drawableColorImageMat = this->drawableImageFF->getImageMat();
	this->origImageFF->getImageMat().copyTo(drawableColorImageMat);

	this->verticalBordersDeductionsClear();
	this->verZonesClear();
	this->horizontalZonesWithMaxPixels.clear();
	


	int nbOfHorZoneToAnalyse = (origFilteredImageMat.rows / horizontalBorderError) + 1;

	// Creation of all horizontal possibles zones.
	shared_ptr<Zone> newHorizontalZone;	
	for(int i = 0 ; i < nbOfHorZoneToAnalyse ; i++)
	{
		int x      = 0;
		int y      = i * horizontalBorderError;
		int width  = origFilteredImageMat.cols;
		int height = this->horizontalBorderError;

		// Verification if created zone isn't an already confirmed horizontal border (possible case when this algorithm is recalled.).
		if(horizontalBordersConfirmed.find(
				 to_string(x)     +"-"
				+to_string(y)     +"-"
				+to_string(width) +"-"
				+to_string(height)) == horizontalBordersConfirmed.end())
		{
			newHorizontalZone = make_shared<Zone>(
									this->origImageFF,
									this->drawableImageFF,
									x,
									y,
									width,
									height
									);
			this->horizontalZonesWithMaxPixels.push_back(newHorizontalZone);
			
		}
	}

	if(this->horizontalZonesWithMaxPixels.size() == 0)
	{
		cout<<"ParcelCounting::horizontalCounting(): no horizontal zones found"<<endl;
		return;
	}

	// Sorting the horizontal zones by number of pixels of each zone (descending order). Since we are counting pixels in a binary image given by Canny's gradient analysis filter, horizontal zones having maxed quantity of pixels with have the highest probability to be enclosing and horizontal grouped border.
	sort(this->horizontalZonesWithMaxPixels.begin() , 
		 this->horizontalZonesWithMaxPixels.end() ,
		 SortByNbOfPixels() );

	
	
	int nbOfMaxPixelsOfHorZones = this->horizontalZonesWithMaxPixels.at(0)->getNbOfPixels() , 
	    indOfLastSimilarZone = 0 , 
	    nbOfPixelsOfCurentHorZone;
	
	double hsimilarness =(double) hZonesSimilarnessInt /1000.0;

	// Looking for all horizontal zones having a similar quantity of 
	// pixels contained in the zone containing nbOfMaxPixelsOfHorZones pixels.
	for(unsigned int i = 1 ; i < this->horizontalZonesWithMaxPixels.size() ; i++ )
	{
		nbOfPixelsOfCurentHorZone = this->horizontalZonesWithMaxPixels.at(i)->getNbOfPixels();
		
		if(nbOfPixelsOfCurentHorZone > (int) (hsimilarness * (double) nbOfMaxPixelsOfHorZones) )
			indOfLastSimilarZone++;
		else
			break;

	}

	// Retaining only horizontal zones similar enough to the most perfect container horizontal border zone.
	this->horizontalZonesWithMaxPixels.resize(indOfLastSimilarZone + 1);
	cout<<"============================================================"<<endl<<endl;
	cout<<"ParcelCounting::horizontalCounting(): hor border zones candidates"<<endl;
	cout<<"============================================================"<<endl<<endl;
	for(unsigned int i = 0 ; i < horizontalZonesWithMaxPixels.size() ; i++)
	{
		Zone* horZone = this->horizontalZonesWithMaxPixels.at(i).get();
		cout.precision(3);
		cout<<*horZone;	
		cout<<"  nbOfPixelsOfMaxBorder : "<<this->horizontalZonesWithMaxPixels.at(0)-> getNbOfPixels()
		   << " similarness : " <<(double) horZone->getNbOfPixels() / this->horizontalZonesWithMaxPixels.at(0)->getNbOfPixels() ;
		cout<<endl;
	}

	sort(this->horizontalZonesWithMaxPixels.begin() , 
		 this->horizontalZonesWithMaxPixels.end(),
		 SortByCoords());


	// Drawing candidate zones enclosing horizontal parcels borders.
	for(unsigned int i = 0 ; i < horizontalZonesWithMaxPixels.size() ; i++)
	{

		Zone* horZone = this->horizontalZonesWithMaxPixels.at(i).get();
		horZone->drawC(255 , 0 , 255);
		
	}

	imshow(this->winNameOfImageToAnalyse, drawableColorImageMat );
	
}



/**
 * @brief      Vertical parcels border's analysis.
 *
 * @details    Once ParcelCounting::horizontalCounting() is validated by the
 *             user, this function will analyses vertical borders between each
 *             pair of horizontal borders found.
 *
 * @param[in]  iDelimiter1          The y-axis of the first horizontal
 *                                  delimiter.
 * @param[in]  iDelimiter2          The y-axis of the second horizontal
 *                                  delimiter.
 * @param[in]  verticalBorderError  The vertical border possible error. In
 *                                  others words this fixes the width of each
 *                                  explored vertical zone. The height will be
 *                                  defined by the vertical distance between
 *                                  iDelimiter1 and iDelimiter2.
 */
void ParcelCounting::horizontalBoundaryAnalysis(int iDelimiter1 , int iDelimiter2 , int verticalBorderError)
{


	Mat origFilteredImageMat = this->origImageFF->getImageMat();
	Mat drawableColorImageMat = this->drawableImageFF->getImageMat();
	CV_Assert(origFilteredImageMat.depth() == CV_8U);
	this->verticalZonesWithMaxPixels.clear();
	
	int nbOfZonesToAnalyse = (origFilteredImageMat.cols / verticalBorderError) + 1;
	double vZoneSimilarnes = static_cast<double>(this->vZonesSimilarnessInt / 1000.0);
	
	cout<<"ParcelCounting::horizontalBoundaryAnalysis(): Size of verticalBordersConfirmed"<<verticalBordersConfirmed.size()<<endl;

	

	

	string vertZoneStrId; int x , y , w , h;
	for(int j = 0 ; j  < nbOfZonesToAnalyse ; j++)
	{
		// We check if the vertical zone isn't a confirmed vertical zone containing a vertical border generally determined by the function searchParcelZones(). This will allow the repetition of the algorithms execution for new vertical border finding.
		x = j * verticalBorderError; 
		y = iDelimiter1;
		w = verticalBorderError;
		h = abs(iDelimiter2  - iDelimiter1) ;
		vertZoneStrId = to_string(x)+"-"+to_string(y)+"-"+to_string(w)+"-"+to_string(h);


		if(   verticalBordersConfirmed.find(vertZoneStrId) ==       			  verticalBordersConfirmed.end() )
		{
			// if is not a confirmed vertical zone containing vertical border, we shall add it to the verticalZonesWithMaxPixels vector for further analysis.
			this->verticalZonesWithMaxPixels.push_back(
				make_shared <Zone>(				
					this->origImageFF       , 
					this->drawableImageFF   ,
					x , y , w , h ));
		}
		
	}


	sort(this->verticalZonesWithMaxPixels.begin() , this->verticalZonesWithMaxPixels.end() , SortByNbOfPixels());	
	unsigned int i=0;
	bool verticalZoneFound = true;
	while(verticalZoneFound)
	{
		verticalZoneFound = false;

	 	if(i < this->verticalZonesWithMaxPixels.size() &&
	 			(i==0 
	 				||(this->verticalZonesWithMaxPixels.at(i)->getNbOfPixels()
	 					> vZoneSimilarnes * static_cast<double>(this->verticalZonesWithMaxPixels.at(0)->getNbOfPixels()))))
		{
			verticalZoneFound = true;
			this->verticalZonesWithMaxPixels.at(i)->drawC(255 , 128 , 0);	
			i++;
		}

	}

	this->verticalZonesWithMaxPixels.resize(i);
	this->verticalZonesWithMaxPixelsByHorId.push_back(move(verticalZonesWithMaxPixels));

}




/**
 * @brief      Filters all the possible vertical zones candidate of enclosing an
 *             vertical parcel border.
 * @details    From all possible vertical zones, the one containing the max
 *             quantity of pixels found in the binary Canny's filtered image, is
 *             compared to the rest. Only the vertical zones which are similar
 *             enough to the vertical zones containing the max quantity of
 *             pixels of enclosed pixels will be kept.
 */
void ParcelCounting::filterVertZonesByMaxColsQtySimilarness()
{
	sort(this->verticalZonesWithMaxPixels.begin() , this->verticalZonesWithMaxPixels.end() , SortByColWithMaxPixels());

	bool verticalBorderFound =true;
	double vBorderSimilarnes = static_cast<double>(this->vBorderSimilarnessInt / 1000.0);

	unsigned int i = 0;
	while(verticalBorderFound)
	{
		verticalBorderFound = false;

	 	if(i < this->verticalZonesWithMaxPixels.size() &&
	 			(i==0 ||(i < this->verticalZonesWithMaxPixels.size() 
	 		&& this->verticalZonesWithMaxPixels.at(i)->getColWithMaxPixelsQty()
				> vBorderSimilarnes * static_cast<double>(this->verticalZonesWithMaxPixels.at(0)->getColWithMaxPixelsQty()))))
		{
			verticalBorderFound = true;
			this->verticalZonesWithMaxPixels.at(i)->drawColWithMaxPixelQtyC(255 , 255 , 51);
			i++;
			
		}

	}

}



/**
 * @brief      Initialization of windows showed, track bars and buttons for user interaction.
 */
void ParcelCounting::init()
{
	cout<<"ParcelCounting::init()"<<endl;
	namedWindow(this->winNameOfImageToAnalyse , WINDOW_NORMAL);
	resizeWindow(this->winNameOfImageToAnalyse , 910 , 700);
	imshow(this->winNameOfImageToAnalyse , this->drawableImageFF->getImageMat());
	
	

	DummyTrackbar::createDummyTrackbar();

	createTrackbar("Horizontal zones similarness" , 
				/*this->winNameOfImageToAnalyse*/"",
                &this->hZonesSimilarnessInt , 
                this->maxCoeff , 
                repeatHorizontalCounting ,
                (void*)this);

	createTrackbar("Vertical zones similarness",
				/*this->winNameOfImageToAnalyse*/"",
				&this->vZonesSimilarnessInt,
				this->maxCoeff,
				repeatVerticalCounting,
				(void*)this);

	createTrackbar("Vertical borders spacing" ,
				/*this->winNameOfImageToAnalyse*/"",
				&this->verticalBorderError,
				this->maxBorderError,
				repeatVerticalCounting,
				(void*)this);

	createTrackbar("Horizontal borders spacing" ,
				/*this->winNameOfImageToAnalyse*/"",
				&this->horizontalBorderError,
				this->maxBorderError,
				repeatHorizontalCounting,
				(void*)this);

	createTrackbar("Horizontal parcel distance  similarness" ,
				/*this->winNameOfImageToAnalyse*/"",
				&this->horDistParcelSimilarnessInt,
				this->maxCoeff,
				repeatVerticalCounting,
				(void*)this);

	createTrackbar("Vertical parcel distance  similarness" ,
				/*this->winNameOfImageToAnalyse*/"",
				&this->verDistParcelSimilarnessInt,
				this->maxCoeff,
				repeatHorizontalCounting,
				(void*)this);
	
	

	createTrackbar("Maximal borders overlapping distance" ,
				/*this->winNameOfImageToAnalyse*/"",
			&this->maxHorBorderOverLappingDistInt,
			this->maxCoeff,
			parcelHorizontalCountingStart,
			(void*)this);



	setMouseCallback(this->winNameOfImageToAnalyse, onMouseParcelCounting, (void*) this );
	
	
	string startHorizontalCountingBName = "Start horizontal counting";
	cvCreateButton(startHorizontalCountingBName.c_str() , parcelHorizontalCountingStart , (void *) this , CV_PUSH_BUTTON , 0); 


	string repeatHorizontalCountingBName = "Repeat horizontal counting";
	cvCreateButton(repeatHorizontalCountingBName.c_str() , repeatHorizontalCounting , (void *) this , CV_PUSH_BUTTON , 0); 

	string horBordersOkBName = "Horizontal borders Ok";
	cvCreateButton(horBordersOkBName.c_str() , horBordersOk , (void *) this , CV_PUSH_BUTTON , 0);


	string repeatVerticalCountingBName = "Repeat vertical counting";
	cvCreateButton(repeatVerticalCountingBName.c_str() , repeatVerticalCounting , (void *) this , CV_PUSH_BUTTON , 0); 

	string verBordersOkBName = "Vertical borders Ok";
	cvCreateButton(verBordersOkBName.c_str() , verBordersOk , (void *) this , CV_PUSH_BUTTON , 0);

}



/**
 * @brief      Repeats the vertical zones mating procedure allowing to use a
 *             less perfect vertical zone as a model of similarness comparison.
 * @details    Repeating of this process will allow the finding of less perfect vertical border
 *             that needs to be taken into consideration.
 *
 * @param[in]  <unnamed>          0
 * @param      parcelCountingPtr  A void* pointer pointing to an ParcelCounting
 *                                instance
 */
void repeatVerticalCounting(int , void *parcelCountingPtr)
{
	ParcelCounting* parcelCounting = static_cast<ParcelCounting*>(parcelCountingPtr);
	
	
	if(parcelCounting->horCountingActive())
		return;
	parcelCounting->colorDrawableImageReset();
	
	parcelCounting->activateVerCounting();
	parcelCounting->verZonesClear();
	parcelCounting->drawParcelZones();
	parcelCounting->horizontalBoundaryAnalysis();

	Mat drawableColorImageMat = parcelCounting->drawableImageFF->getImageMat();
	imshow(parcelCounting->winNameOfImageToAnalyse , drawableColorImageMat );
}

/**
 * @brief      Using the confirmed horizontal zones deduced by
 *             ParcelCounting::horizontalCounting(int), this method will analyze
 *             each horizontal zone in search of vertical borders zones.
 */
void ParcelCounting::horizontalBoundaryAnalysis()
{
	map<string, shared_ptr<Zone>>::iterator it;

	for ( it =  this->horBoundaryZones.begin(); it != this->horBoundaryZones.end(); it++ )
	{
	    
	    this->horizontalBoundaryAnalysis(it->second->getY() , it->second->getY() + it->second->getH() , this->verticalBorderError);
	}
	
}

/**
 * @brief      Repeats the horizontal zones mating procedure allowing to use a
 *             less perfect horizontal zone as a model of similarness
 *             comparison.
 * @details    Repeating of this process will allow the finding of less perfect
 *             horizontal parcel border that needs to be taken into
 *             consideration.
 *
 * @param[in]  <unnamed>          0
 * @param      parcelCountingPtr  A void* pointer pointing to an ParcelCounting instance
 */
void repeatHorizontalCounting(int , void *parcelCountingPtr)
{
	ParcelCounting* parcelCounting = static_cast<ParcelCounting*>(parcelCountingPtr);

	if(parcelCounting->verCountingActive())
	{
		parcelCounting->horizontalZonesClear();
		parcelCounting->horizontalDeductionsClear();
		cout<<"ParcelCounting::repeatHorizontalCounting()"<<endl;
	}

	parcelCounting->activateHorCounting();		
	parcelCounting->horizontalCounting(parcelCounting->horizontalBorderError);
	parcelCounting->drawConfirmedHorizontalZones();
	Mat drawableColorImageMat = parcelCounting->drawableImageFF->getImageMat();
	imshow(parcelCounting->winNameOfImageToAnalyse , drawableColorImageMat );
}


/**
 * @brief      cvButton callback allowing to validate the vertical parcel border deductions.
 *
 * @param[in]  <unnamed>          0
 * @param      parcelCountingPtr  A void* pointer pointing to an ParcelCounting instance
 */
void verBordersOk(int , void *parcelCountingPtr)
{
	ParcelCounting* parcelCounting = static_cast<ParcelCounting*>(parcelCountingPtr);

	if(parcelCounting->horCountingActive())
		return;
	parcelCounting->colorDrawableImageReset();
	parcelCounting->drawParcelZones();
	imshow(parcelCounting->winNameOfImageToAnalyse , parcelCounting->drawableImageFF->getImageMat());


}


/**
 * @brief      cvButton callback allowing to validate the horizontal parcel border deductions.
 *
 * @param[in]  <unnamed>          0
 * @param      parcelCountingPtr  A void* pointer pointing to an ParcelCounting instance
 */
void horBordersOk(int , void *parcelCountingPtr)
{
	ParcelCounting* parcelCounting = static_cast<ParcelCounting*>(parcelCountingPtr);
	parcelCounting->colorDrawableImageReset();
	parcelCounting->drawHorBoundaries();
	imshow(parcelCounting->winNameOfImageToAnalyse , parcelCounting->drawableImageFF->getImageMat());
	parcelCounting->activateVerCounting();
	parcelCounting->horizontalBoundaryAnalysis();
	

	imshow(parcelCounting->winNameOfImageToAnalyse , parcelCounting->drawableImageFF->getImageMat());


}

/**
 * @brief      Draws in ParcelCounting::colorDrawableImage all the horizontal
 *             deduced (by ParcelCounting::searchHorBoundaries()) boundaries.
 */
void ParcelCounting::drawHorBoundaries()
{
	map<string, shared_ptr<Zone>>::iterator it;

	for ( it = this->horBoundaryZones.begin(); it != this->horBoundaryZones.end(); it++ )
	{
	    it->second->drawC(0 , 0 , 255);
	}
}

/**
 * @brief      Execute behavior of this ParcelCounting instance (used by State
 *             class) .
 * @details    Execute behavior composed of the reinitialization and
 *             recalculation of horizontal parcels borders deductions.
 */
void ParcelCounting::execBehavior()
{   
	cout<<"============================================================"<<endl<<endl<<endl<<endl;;
	cout<<"ParcelCounting::execBehavior()"<<endl;
	cout<<endl<<endl<<endl<<endl<<"============================================================"<<endl;
	this->verticalBordersDeductionsClear();
	this->verZonesClear();
	this->horizontalZonesWithMaxPixels.clear();
	this->horizontalBordersConfirmed.clear();
	this->horBoundaryZones.clear();

	this->horizontalCounting(this->horizontalBorderError);	



	//DEBUG PURPOSE ONLY
		if(this->horFirst)
		{
			this->horFirst = false;
			onMouseParcelCounting(
				EVENT_LBUTTONUP, 
				horizontalZonesWithMaxPixels.at(1)->getX() , 
				horizontalZonesWithMaxPixels.at(1)->getY() , 
				0,  
				(void *) this
				);

			onMouseParcelCounting(
				EVENT_LBUTTONUP,
				horizontalZonesWithMaxPixels.at(2)->getX() ,
				horizontalZonesWithMaxPixels.at(2)->getY() ,	
				0,
				(void *) this
				);
			
		}
	//END DEBUG PURPOSE ONLY
}



/**
 * @brief      cvButton callback functions to start the horizontal parcel
 *             counting .
 *
 * @param[in]  <unnamed>  0
 * @param      userdata   A void* pointer pointing to a ParcelCounting instance.
 */
void parcelHorizontalCountingStart(int , void *userdata)
{
	ParcelCounting * parcelCounting = static_cast<ParcelCounting*>(userdata);
	
	if(parcelCounting != nullptr)
	{
		State::execState(parcelCounting->getId());
	}
}


/**
 * @brief      Using all the  horizontal zones founded considered as horizontal
 *             parcels borders wrappers where delimiters will be deduced. This
 *             procedure looks for horizontal pairs zones for the vertical
 *             analysis preparation.
 */
void ParcelCounting::searchHorBoundaries()
{	
	cout<<"ParcelCounting::horizontalCounting() : horizontalBordersConfirme pre SHB:"<<endl;
	map<string, shared_ptr<Zone>>::iterator it;
	for ( it =  this->horizontalBordersConfirmed.begin(); it != this->horizontalBordersConfirmed.end(); it++ )
	{
	    cout<<*it->second;
	}

	Zone* zone1 , * zone2; 
	Line verDistLineFound(this->origImageFF , this->drawableImageFF , 0 , 0 , 0 , 0 );
	sort(this->horizontalZonesWithMaxPixels.begin() , this->horizontalZonesWithMaxPixels.end() , SortByCoords() );

	if(this->zonesForVertDistDeducPriority)
	{
		this->createIfValidNewHorBoundaryZone(this->zone1ForVertDistDeduc , this->zone2ForVertDistDeduc);
		this->zonesForVertDistDeducPriority = false;
	}
	for(unsigned int i1= 0 ; i1 < this->horizontalZonesWithMaxPixels.size() - 1 ; i1++)
	{
		zone1 = this->horizontalZonesWithMaxPixels.at(i1).get();
		if(this->isAConfirmedHorizontalZone(*zone1))
			continue;
		for(unsigned int i2 = i1 + 1 ; i2 < this->horizontalZonesWithMaxPixels.size(); i2++)
		{
			zone2 = this->horizontalZonesWithMaxPixels.at(i2).get();
			if(this->isAConfirmedHorizontalZone(*zone2))
				continue;

			verDistLineFound.setp1p2coords(
						zone1->getX() + (zone1->getW() / 2.0) , 
						zone1->getY() + (zone1->getH())  ,
						zone2->getX() + (zone2->getW() / 2.0) ,
						zone2->getY());


			// dissCoeff: coefficient of dissimilarity between the vertical distance model size and the vertical distance found. The more dissCoeff is near to 1.0, the more the distances aren't similar.
			double dissCoeff = abs(verDistLineFound.eucledianDist() - modelLineVerDistParcel.eucledianDist()) / 
					max<double>(verDistLineFound.eucledianDist() , modelLineVerDistParcel.eucledianDist()) ;

			// The vertical distance similarity coefficient defined in between 0 and 1 that imposes the level of similitude between the vertical distances comparisons. 
			double verDistParcelSimilarness = this->verDistParcelSimilarnessInt / 1000.0;
			

			// cout<<endl<<"\t zone1"<<*zone1;
			// cout<<"\t zone2"<<*zone2;
			// cout<<"\t foundEucDist="<< verDistLineFound.eucledianDist()
			//     <<" modelEucDist="  << modelLineVerDistParcel.eucledianDist() 
			//     <<" vertDistParcelSim" << verDistParcelSimilarness
			// 	<<" coef="<<(abs(verDistLineFound.eucledianDist() - modelLineVerDistParcel.eucledianDist()) / 
			// 			max<double>(verDistLineFound.eucledianDist() , modelLineVerDistParcel.eucledianDist()) )
			// 	<<" hordistParcelSimilarness - 1="<<abs(verDistParcelSimilarness - 1)<<endl;

				


			// If the border's zones distance is longer than the model distance of vertical borders, break to loop and that the vertical line distance found is greater than the model line distance => zone2 is to far away from zone1.
			if(dissCoeff > abs( verDistParcelSimilarness - 1) 
				&& verDistLineFound.eucledianDist() > modelLineVerDistParcel.eucledianDist() )
			{
				cout<<"ParcelCounting::searchHorBoundaries() : break"<<endl;
				break;
			}

			// If the vertical line distance found is inferior than the model line distance, we keep looking from the same upside border (zone1) changing only the downside border (zone2).
			else if (dissCoeff > abs( verDistParcelSimilarness - 1) &&verDistLineFound.eucledianDist() < modelLineVerDistParcel.eucledianDist() )
			{
				cout<<"ParcelCounting::searchHorBoundaries() : continue"<<endl;
				continue;
			}

			// The border's zones are close enough.
			if ((double) dissCoeff < abs((double) verDistParcelSimilarness - 1))
			{
				verDistLineFound.drawC(255 , 51 , 153);
				
				if(this->createIfValidNewHorBoundaryZone(
					this->horizontalZonesWithMaxPixels.at(i1) , 
					this->horizontalZonesWithMaxPixels.at(i2)))
				{
					i1 = i2 ;
					break;
				}


				
			}


		}
	}

	cout<<"ParcelCounting::horizontalCounting() : horizontalBordersConfirmed post : "<<endl;
	
	for ( it =  this->horizontalBordersConfirmed.begin(); it != this->horizontalBordersConfirmed.end(); it++ )
	{
	    cout<<*it->second;
	}


}

bool ParcelCounting::createIfValidNewHorBoundaryZone(shared_ptr<Zone> horZone1 , shared_ptr<Zone> horZone2)
{
	Mat drawableFilteredImageMat = this->drawableImageFF->getFilteredImageMat();
				
	int x = horZone1->getX();
	int y = horZone1->getY() - this->horizontalBorderError;
	if(y < 0)
		y = 0; 
	int w = horZone1->getX() + horZone1->getW();
	int h;

	if(horZone1->getY() < horZone2->getY())
	{
		h = horZone2->getH() + horZone2->getY() - horZone1->getY() + (2 * this->horizontalBorderError);
	}
	else
	{
		h = horZone1->getH() + horZone1->getY() - horZone2->getY() + (2 * this->horizontalBorderError);
	}
	if(y + h >= drawableFilteredImageMat.rows)
		h = y + h - (y + h - drawableFilteredImageMat.rows) - 1;

	Zone candidateHorBoundaryZone (
		this->origImageFF      , 
		this->drawableImageFF  ,
		x , y , w , h );
		
		if(!this->isHorZoneInConfirmedBoundaryZone(candidateHorBoundaryZone))
		{
			horZone1->drawC(0 , 0 , 255);
			horZone2->drawC(0 , 0 , 255);
			horZone1->drawHorInBetweenSurface(*horZone2 , this->horizontalBorderError);
			this->addNewHorBorderConfirmed(horZone1);
			this->addNewHorBorderConfirmed(horZone2);

			
			this->addNewHorBoundaryZoneConfirmed(candidateHorBoundaryZone);
			
			return true;
		}

	return false;
}

/**
 * @brief      Adds a new confirmed horizontal border zone in the designated
 *             manager structure.
 *
 * @param[in]  zone  The confirmed horizontal zone enclosing an horizontal group
 *                   of pixel designating a majority of a group of horizontal
 *                   parcel borders.
 */
void ParcelCounting::addNewHorBorderConfirmed(shared_ptr<Zone> zone)
{
	cout<<" ParcelCounting::addNewHorBorderConfirmed() :"<<endl;
	cout<<" inserting horizontalBorderConfirmed: "<<*zone;
	
	this->horizontalBordersConfirmed[zone->toString()] = zone;
	cout<<" horizontalBordersConfirmed just after insertion : "<<endl;
	map<string,shared_ptr<Zone>>::iterator it;
	for ( it =  this->horizontalBordersConfirmed.begin(); it != this->horizontalBordersConfirmed.end(); it++ )
	{
		cout<<it->second->coordsToString()<<endl;;
		cout<<it->first<<endl;
	    cout<<*it->second;
	}
}

/**
 * @brief      Adds a new confirmed horizontal border zone in the designated
 *             manager structure from a <b> NONE INSTANTIATED </b> Zone object.
 *
 * @param[in]  zone  The confirmed <b> NONE INSTANTIATED </b> horizontal zone enclosing
 *                   an horizontal group of pixel designating a majority of a
 *                   group of horizontal parcel borders.
 */
void ParcelCounting::addNewHorBoundaryZoneConfirmed(Zone& zone)
{	
	shared_ptr<Zone> horBoundaryZone = make_shared <Zone>(
				this->origImageFF      , 
				this->drawableImageFF  ,
				zone.getX() , zone.getY() , zone.getW() , zone.getH());

	this->addNewHorBoundaryZoneConfirmed(horBoundaryZone);
}


/**
 * @brief      Adds a new confirmed horizontal boundary zone enclosing a
 *             horizontal group of parcels
 *
 * @param[in]  horBoundaryZone  The new confirmed horizontal boundary zone.
 */
void ParcelCounting::addNewHorBoundaryZoneConfirmed(shared_ptr<Zone> horBoundaryZone)
{
	cout<<"inserting new hor boundary zone "<<*horBoundaryZone;
	cout<<"\t\t new boundary zone to string:"<<horBoundaryZone->coordsToString()<<endl;
	this->horBoundaryZones[horBoundaryZone->coordsToString()]=horBoundaryZone;


	cout<<"horBoundaryZones just after insertion : "<<endl;
	map<string,shared_ptr<Zone>>::iterator it;
	for ( it =  this->horBoundaryZones.begin(); it != this->horBoundaryZones.end(); it++ )
	{
		cout<<it->second->coordsToString()<<endl;;
		cout<<it->first<<endl;
	    cout<<*it->second;
	}
}

/**
 * @brief      Once vertical border parcel analyses is validated by the user,
 *             the final search for vertical zones mating is realized by this
 *             function.
 *
 * @details    With the use of an horizontal model line
 *             ParcelCounting::modelLineHorDistParcel (defined by the user)
 *             defining the average distance between two parcel vertical borders,
 *             this procedure will look for pairs to find the vertical
 *             delimiters of each parcel.
 */
void ParcelCounting::searchParcelZones()
{
	Zone* zone1 , * zone2; 
	Line horDistLineFound(this->origImageFF , this->drawableImageFF , 0 , 0 , 0 , 0 );

	double horDistParcelSimilarness = static_cast<double>(this->horDistParcelSimilarnessInt / 1000.0);
	for(unsigned int h = 0 ; h < this->verticalZonesWithMaxPixelsByHorId.size() ; h++)
	{
		sort(this->verticalZonesWithMaxPixelsByHorId.at(h).begin() , 
			 this->verticalZonesWithMaxPixelsByHorId.at(h).end()   ,
			 SortByCoords());
		for(unsigned int i = 0 ; i < this->verticalZonesWithMaxPixelsByHorId.at(h).size() - 1; i++)
		{
			// We compare each vertical zone found in the h-th horizontal
			// delimited zone defined by this->horizontalCounting(..).
			zone1 = this->verticalZonesWithMaxPixelsByHorId.at(h).at(i).get();
			
			for(unsigned int j = i + 1 ; j < this->verticalZonesWithMaxPixelsByHorId.at(h).size() ; j++)
			{	
				cout<<endl<<"ParcelCounting::searchParcelZones(): h="<<h<<" i="<<i<<" j="<<j<<endl;

				zone2 = this->verticalZonesWithMaxPixelsByHorId.at(h).at(j).get();
				
				// Saving the line found between two border's zones.
				horDistLineFound.setp1p2coords(
						zone1->getColIndexWithMaxPixels() + zone1->getX() , 
						zone1->getY() + (zone1->getH()/2.0)  ,
						zone2->getColIndexWithMaxPixels() + zone2->getX() ,
						zone2->getY() + (zone2->getH()/2.0));
				cout<<"\t zone1"<<*zone1;
				cout<<"\t zone2"<<*zone2;
				cout<<"\t foundEucDist="<< horDistLineFound.eucledianDist()
					<<" modelEucDist="<< modelLineHorDistParcel.eucledianDist() 
					<<" coef="<<(abs(horDistLineFound.eucledianDist() - modelLineHorDistParcel.eucledianDist()) / 
						max<double>(horDistLineFound.eucledianDist() , modelLineHorDistParcel.eucledianDist()) )
					<<" hordistParcelSimilarness - 1="<<abs(horDistParcelSimilarness - 1)<<endl;

				
				// dissCoeff: coefficient of dissimilarity between the horizontal distance model size and the horizontal distance found. The more dissCoeff is near to 1.0, the more the distances aren't similar.
				double dissCoeff = abs(horDistLineFound.eucledianDist() - modelLineHorDistParcel.eucledianDist()) / 
						max<double>(horDistLineFound.eucledianDist() , modelLineHorDistParcel.eucledianDist()) ;
				// If the border's zones distance is longer than the model distance of vertical borders, break to loop and that the horizontal line distance found is greater than the model line distance => zone2 is to far away from zone1.
				if(dissCoeff > abs( horDistParcelSimilarness - 1) 
					&& horDistLineFound.eucledianDist() > modelLineHorDistParcel.eucledianDist() )
				{
					cout<<"ParcelCounting::searchParcelZones() : break"<<endl;
					break;
				}

				// If the horizontal line distance found is inferior than the model line distance, we keep looking from the same left border (zone1) changing only the right (zone2) chosen border 
				else if (dissCoeff > abs( horDistParcelSimilarness - 1) &&horDistLineFound.eucledianDist() < modelLineHorDistParcel.eucledianDist() )
				{
					continue;
				}

				// The border's zones are close enough. 
				else if (dissCoeff < abs(horDistParcelSimilarness - 1))
				{
					horDistLineFound.drawC(0 , 255 , 255);
					
					this->verticalBordersConfirmed[zone1->toString()] =this->verticalZonesWithMaxPixelsByHorId.at(h).at(i);
					this->verticalBordersConfirmed[zone2->toString()] =this->verticalZonesWithMaxPixelsByHorId.at(h).at(j);

					int x = zone1->getColIndexWithMaxPixels() + zone1->getX();
					int y = zone1->getY(); 
					int w = (zone2->getX() + zone2->getW()) - (zone1->getX());
					int h = zone1->getH();
					
					shared_ptr<Zone> parcelZone = make_shared <Zone>(
					this->origImageFF     , 
					this->drawableImageFF ,
					x , y , w , h );
					
					if(!this->isVertZoneInConfirmedPZone(*parcelZone))
					{
						zone1->drawC(0 , 255 , 0);
						zone2->drawC(0 , 255 , 0);
						// Inserting deduced parcelZone ;
							this->parcelZones[parcelZone->coordsToString()] = parcelZone;
						i = j ;
						break;
					}


					
				}
			}

		}
	}
}


/**
 * @brief      Intermediate between the parcelZones map structure and integer
 *             coordinates allowing to determine if the given point (x,y) is in
 *             an existing deduced parcel zone.
 *
 * @param[in]  x     x-axis of the point
 * @param[in]  y     y-axis of the point
 *
 * @return     True if Point(x,y) is in a parcel zone, false otherwise.
 */
bool ParcelCounting::inParcelZone(int x , int y)
{
	return ! (this->parcelZones.find(to_string(x)+"-"+to_string(y)) == this->parcelZones.end());
}


/**
 * @brief      Gets the parcel zone.enclosing the point Point(x,y)
 *
 * @param[in]  x     x-axis of the point
 * @param[in]  y     y-axis of the point
 *
 * @return     The parcel zone enclosing the Point(x,y) if it exits, an empty
 *             shared_ptr<Zone> instance otherwise.
 */
shared_ptr<Zone> ParcelCounting::getParcelZone(int x , int y)
{

	map<string,shared_ptr<Zone>>::iterator it= this->parcelZones.find(to_string(x)+"-"+to_string(y)); ;
	
	if(it != this->parcelZones.end())
		return it->second;
		
	else
		return shared_ptr<Zone>();
}


/**
 * @brief      Gets the horizontal boundary zone.enclosing the point Point(x,y)
 *
 * @param[in]  x     x-axis of the point
 * @param[in]  y     y-axis of the point
 *
 * @return     The horizontal boundary zone enclosing the Point(x,y) if it
 *             exits, an empty shared_ptr<Zone> instance otherwise.
 */
shared_ptr<Zone> ParcelCounting::getHorBoundaryZone(int x , int y)
{
	map<string,shared_ptr<Zone>>::iterator it = this->horBoundaryZones.find(to_string(x)+"-"+to_string(y));
	
	if(it != this->horBoundaryZones.end())
		return it->second;
		
	else
		return shared_ptr<Zone>();	
}



/**
 * @brief      Gets the vertical zone enclosing p if this one exits in the
 *             ParcelCounting::verticalZonesWithMaxPixelsByHorId.
 *
 * @param      p     A cv Point.
 *
 * @return     The vertical zone.if founded, nullptr otherwise
 */
Zone * ParcelCounting::getVZone(Point &p)
{
	Zone *zone;
	cout<<"ParcelCounting::getVZone().size():"<<this->verticalZonesWithMaxPixelsByHorId.size()<<endl;
	for(unsigned int i = 0 ; i  < this->verticalZonesWithMaxPixelsByHorId.size() ; i++ )
	{
		for(unsigned int j = 0 ; j < this->verticalZonesWithMaxPixelsByHorId.at(i).size(); j++)
		{
			zone = this->verticalZonesWithMaxPixelsByHorId.at(i).at(j).get();
			if(p.x >= zone->getX() && p.x < zone->getX() + zone->getW()
				&& p.y >= zone->getY() && p.y < zone->getY() + zone->getH() )
				return zone;
		}
	}
	return nullptr;
}

/**
 * @brief      Gets the horizontal zone enclosing p if this one exits in the
 *             ParcelCounting::horizontalZonesWithMaxPixels.
 *
 * @param      p     A cv Point.
 *
 * @return     The horizontal zone if founded, nullptr otherwise.
 */
shared_ptr<Zone>  ParcelCounting::getHZone(Point &p)
{
	shared_ptr<Zone> zone;

	for(unsigned int i = 0 ; i  < this->horizontalZonesWithMaxPixels.size() ; i++ )
	{
		zone = this->horizontalZonesWithMaxPixels.at(i);
		if(p.x >= zone->getX() && p.x < zone->getX() + zone->getW()
				&& p.y >= zone->getY() && p.y < zone->getY() + zone->getH() )
				return zone;
	}
	return nullptr;
}

/**
 * @brief      Allows the user to help the research of the parcel zones
 *             contained in the image to analyze.
 *
 * @param[in]  event      The event
 * @param[in]  x          { parameter_description }
 * @param[in]  y          { parameter_description }
 * @param[in]  <unnamed>  { parameter_description }
 */
void onMouseParcelCounting( int event, int x, int y, int, void* parcelCountingPtr)
{
    
	ParcelCounting *parcelCounting = (ParcelCounting*) parcelCountingPtr ;

	
	if(!parcelCounting)
		return;
    
    switch( event )
    {
    case EVENT_LBUTTONDOWN:
        {
            
        }
        break;
    case EVENT_LBUTTONUP:
	    {

    		
			// If vertical zones counting is activated
    		if(parcelCounting->verCountingActive())
			{
				if(!parcelCounting->pUserHorDist1set)
				{
						
					// Allows the user to chose the first of two vertical zones to
					// define the ParcelCounting::zone1ForHorDistDeduc associate to the
					// instance parcelCounting. This will be the first of two delimiters
					// that will allow to keep a model distance between two
					// vertical parcels borders for future complete parcel deductions.
				
				
					Point pUserHorDist1(x , y);
					parcelCounting->zone1ForHorDistDeduc= parcelCounting->getVZone(pUserHorDist1);
					
					if(parcelCounting->zone1ForHorDistDeduc != nullptr)
					{
						parcelCounting->pUserHorDist1set = true;
			    		parcelCounting->zone1ForHorDistDeduc->drawC(0 , 255 , 255);
						Mat drawableColorImageMat = parcelCounting->drawableImageFF->getImageMat();	    		
			    		imshow(parcelCounting->winNameOfImageToAnalyse , drawableColorImageMat);
					}	
					else
					{
						cout<<"ParcelCounting onMouseParcelCounting() : nullptr found"<<endl;
					}

				}
		    	else
		    	{
					// At this stage the first zone ParcelCounting::zone1ForHorDistDeduc has been selected by the user. The second one, if valid, will allow to determinate the model distance between the parcel's vertical borders.
		    		Point pUserHorDist2(x , y);
		    		parcelCounting->zone2ForHorDistDeduc= parcelCounting->getVZone(pUserHorDist2);
	
		    		if(parcelCounting->zone2ForHorDistDeduc != nullptr)
		    		{	
		    			Zone *zone1ForHorDistDeduc , *zone2ForHorDistDeduc;
	
		    			zone1ForHorDistDeduc = parcelCounting->zone1ForHorDistDeduc;
		    			zone2ForHorDistDeduc = parcelCounting->zone2ForHorDistDeduc;
	
	
			    		Mat drawableColorImageMat = parcelCounting->drawableImageFF->getImageMat();
			    		
			    		
			    		parcelCounting->pUserHorDist1set = false;
			    		zone2ForHorDistDeduc->drawC(0 , 255 , 255);
	
			    		cout<<"onMouseParcelCounting() : zone1 mouse selected"<<*zone1ForHorDistDeduc;
			    		cout<<"onMouseParcelCounting() : zone2 mouse selected"<<*zone2ForHorDistDeduc;
			    		
			    		if(zone1ForHorDistDeduc->getX() < zone2ForHorDistDeduc->getX() || zone1ForHorDistDeduc->getX() > zone2ForHorDistDeduc->getX())
			    		{
	
			    			Point p1(
			    				zone1ForHorDistDeduc->getColIndexWithMaxPixels() + zone1ForHorDistDeduc->getX() ,
			    				zone1ForHorDistDeduc->getY() + zone1ForHorDistDeduc->getH()/2.0) , 
			    				  p2(
			    				zone2ForHorDistDeduc->getColIndexWithMaxPixels() + zone2ForHorDistDeduc->getX() ,
			    				zone2ForHorDistDeduc->getY() + zone2ForHorDistDeduc->getH()/2.0);
	
	
			    			
			    			parcelCounting->modelLineHorDistParcel.setp1p2coords(p1.x , p1.y , p2.x , p2.y);
			    			parcelCounting->modelLineHorDistParcel.drawC(0 , 255 , 255);

			    			// Once the model line is set, the mating process of vertical zones
			    			// is started.
							parcelCounting->searchParcelZones();
	
			    		}
			    		// Deselecting first vertical zone choice.
			    		else if (*zone1ForHorDistDeduc == *zone2ForHorDistDeduc)
			    		{
			    			zone1ForHorDistDeduc->drawC(255 , 0 , 255);
							parcelCounting->pUserHorDist1set = false;
			    		}
	
	
						imshow(parcelCounting->winNameOfImageToAnalyse , drawableColorImageMat);
		    		}
	
		    	}
			}
			// If horizontal zones counting is activated
			else if(parcelCounting->horCountingActive())
			{

				// Allows the user to chose the first of two horizontal zones to
				// define the ParcelCounting::zone1ForVerDistDeduc associate to the
				// instance parcelCounting. This will be the first of two delimiters
				// that will allow to keep a model distance between two
				// horizontal parcel borders for future complete parcel deductions.
				
				if(!parcelCounting->pUserVertDist1set)
				{
		    		Point pUserVertDist1(x , y);
		    		parcelCounting->zone1ForVertDistDeduc= parcelCounting->getHZone(pUserVertDist1);
		    		
		    		if(parcelCounting->zone1ForVertDistDeduc != nullptr)
		    		{
		    			parcelCounting->pUserVertDist1set = true;
			    		parcelCounting->zone1ForVertDistDeduc->drawC(0 , 255 , 255);
						Mat drawableColorImageMat = parcelCounting->drawableImageFF->getImageMat();	    		
			    		imshow(parcelCounting->winNameOfImageToAnalyse , drawableColorImageMat);
		    		}	
		    		else
					{
						cout<<"ParcelCounting onMouseParcelCounting() : nullptr found"<<endl;
					}

				}
				else
				{
					// At this stage the first zone ParcelCounting::zone1ForVerDistDeduc has been selected by the user. The second one, if valid, will allow to determinate the model distance between the parcel's horizontal borders.
					cout<<"ParcelCounting onMouse() pUserVertDist1 set!"<<endl;
					Point pUserVertDist2(x , y);
		    		parcelCounting->zone2ForVertDistDeduc= parcelCounting->getHZone(pUserVertDist2);
		    		
		    		if(parcelCounting->zone2ForVertDistDeduc != nullptr)
		    		{	
		    			
		    			shared_ptr<Zone> zone1ForVertDistDeduc , zone2ForVertDistDeduc;

		    			zone1ForVertDistDeduc = parcelCounting->zone1ForVertDistDeduc;
		    			zone2ForVertDistDeduc = parcelCounting->zone2ForVertDistDeduc;

		    			
			    		Mat drawableColorImageMat = parcelCounting->drawableImageFF->getImageMat();
			    		
			    		
			    		parcelCounting->pUserVertDist1set = false;
			    		zone2ForVertDistDeduc->drawC(0 , 255 , 255);
			    		
			    		
			    		
			    		if(zone1ForVertDistDeduc->getY() > zone2ForVertDistDeduc->getY() )
			    		{
			    			
			    			shared_ptr<Zone> tempZone = zone1ForVertDistDeduc;
			    			zone1ForVertDistDeduc = zone2ForVertDistDeduc;
			    			zone2ForVertDistDeduc = tempZone;
			    		}

			    		if(zone1ForVertDistDeduc->getY() < zone2ForVertDistDeduc->getY())
			    		{
			    			
			    			Point p1(
			    				zone1ForVertDistDeduc->getX() + zone1ForVertDistDeduc->getW()/2.0 ,
			    				zone1ForVertDistDeduc->getY() + zone1ForVertDistDeduc->getH()) , 
			    				  p2(
			    				zone2ForVertDistDeduc->getX() +  zone2ForVertDistDeduc->getW()/2.0,
			    				zone2ForVertDistDeduc->getY());


			    			
			    			parcelCounting->modelLineVerDistParcel.setp1p2coords(p1.x , p1.y , p2.x , p2.y);
			    			
			    			
			    			parcelCounting->modelLineVerDistParcel.drawC(255 , 51 , 153);

			    			// Once the model line is set, the mating process of horizontal zones
		    				// is started.
								
							cout<<"================================"<<endl<<endl<<endl;
							cout<<"ParcelCounting onMouseParcelCounting() : searchHorBoundaries"<<endl<<endl<<endl;;

							parcelCounting->zonesForVertDistDeducPriority = true;

							parcelCounting->searchHorBoundaries();
			


			    		}
			    		// Deselecting first horizontal zone choice.
			    		else if (*zone1ForVertDistDeduc == *zone2ForVertDistDeduc)
			    		{
			    			
			    			zone1ForVertDistDeduc->drawC(255 , 0 , 255);
			    			parcelCounting->pUserVertDist1set = false;

			    		}


						imshow(parcelCounting->winNameOfImageToAnalyse , drawableColorImageMat);
		    		}
		    		else
					{
						cout<<"ParcelCounting onMouseParcelCounting() : nullptr found"<<endl;
					}
				}
	    	}
		}
    	break;
    }
}



/**
 * @brief      Clear all the vertical borders confirmed by ParcelCounting::searchParcelZones().
 */
void ParcelCounting::verticalBordersConfirmedClear()
{
	this->verticalBordersConfirmed.clear();
}



/**
 * @brief      Clear all vertical zones deductions.
 */
void ParcelCounting::verticalBordersDeductionsClear()
{
	this->verticalBordersConfirmedClear();
	
	this->parcelZones.clear();
}

/**
 * @brief      Clear horizontal zones with max pixels quantity filled by
 *             ParcelCounting::horizontalCounting(int)
 */
void ParcelCounting::horizontalZonesClear()
{
	this->horizontalZonesWithMaxPixels.clear();
}


/**
 * @brief      Clear all horizontal deductions.
 */
void ParcelCounting::horizontalDeductionsClear()
{
	this->horizontalBordersConfirmed.clear();
	this->horBoundaryZones.clear();
}

/**
 * @brief      Draws confirmed vertical zones in the ParcelCounting::colorDrawableImage Image.
 */
void ParcelCounting::drawConfirmedVerticalZones()
{

	map<string, shared_ptr<Zone>>::iterator it;

	for ( it = this->verticalBordersConfirmed.begin(); it != this->verticalBordersConfirmed.end(); it++ )
	{
	    it->second->drawC(0 , 255 , 0);
	}
}

/**
 * @brief      Draws confirmed horizontal zones in the ParcelCounting::colorDrawableImage Image.
 */
void ParcelCounting::drawConfirmedHorizontalZones()
{
	map<string, shared_ptr<Zone>>::iterator it , it2;
	unsigned int i = 0;
	

	RNG rng(12345);
    Scalar color;
	

	for ( it = this->horizontalBordersConfirmed.begin(); it != this->horizontalBordersConfirmed.end(); it++ )
	{
		if(i % 2 == 0)
		{
			color = Scalar(rng.uniform(204, 102), rng.uniform(204, 102), rng.uniform(204, 102));	
			if(color[0]>= 200 )
			{
				color[2] = 0;
			}		
			it2 = it;
			it2++;

			it->second->drawHorInBetweenSurface(*it2->second.get() , this->horizontalBorderError);
		}	
	    it->second->drawC(color[0] , color[1] , color[2]);


	    i++;
	}	
}


/**
 * @brief      Draws deduced parcel zones.
 */
void ParcelCounting::drawParcelZones()
{
	map<string, shared_ptr<Zone>>::iterator it;

	for ( it = this->parcelZones.begin(); it != this->parcelZones.end(); it++ )
	{
	    it->second->drawC(0 , 0 , 255);
	}	
}


/**
 * @brief      Reset the ParcelCounting::colorDrawableImage attribut to its
 *             original form. (Mainly with out selections drawn in it).
 */
void ParcelCounting::colorDrawableImageReset()
{
	this->origImageFF->getImageMat().copyTo(this->drawableImageFF->getImageMat());
}

/**
 * @brief      Clear vertical zones found by
 *             ParcelCounting::horizontalBoundaryAnalysis(int iDelimiter1 , int
 *             , int)
 */
void ParcelCounting::verZonesClear()
{

	this->verticalZonesWithMaxPixelsByHorId.clear();
	this->verticalZonesWithMaxPixels.clear();
}


/**
 * @brief      Determines if vertical zone is in confirmed parcel zone.
 * @details    This procedure allows to avoid creating overlapping vertical
 *             zones.
 *
 * @param      z     Vertical zone that will be verified if it's overlapping an existing
 *                   confirmed parcel zone
 *
 * @return     True if vertical zone in confirmed parcel zone, False otherwise.
 */
bool ParcelCounting::isVertZoneInConfirmedPZone(Zone &z)
{
	double horDistParcelSimilarness = this->horDistParcelSimilarnessInt / 1000.0;

	
	// We define j1 as the starting abscissa point from where the search of an overlapping neighbour. (z.getX() - j1) corresponds to the maximal horizontal distance to the left (from z.getX()) where the origin of a possible overlapping z could be found.
	int j1= z.getX() - 
		(   (double)
			this->modelLineHorDistParcel.eucledianDist()
			+
			(
				(double)
				this->modelLineHorDistParcel.eucledianDist() 
				* (abs(horDistParcelSimilarness - 1)) - 1
			)
		);
	
	if(j1 < 0 ) j1 = 0;

	
	

	double maxHorBorderOverlappingDist = this->maxHorBorderOverLappingDistInt / 1000.0;

	// Looking from the origin of the zone z towards left, if there's another parcel zone that could overlap z. If found, we'll refer to it as a sandwich zone.
	shared_ptr<Zone> sandwichZoneCandidate;
	for(; j1< z.getX() ; j1++)
	{
		sandwichZoneCandidate = this->getParcelZone(j1 , z.getY());
		if(sandwichZoneCandidate)
		{
			cout<<"\tfound sandwichZoneCandidate:"<<*sandwichZoneCandidate<<endl;
			

			if(z.getX() <= sandwichZoneCandidate->getX() + sandwichZoneCandidate->getW() && z.getX() >= sandwichZoneCandidate->getX())
			{

				// The x distance of the zones intersection.
				int d = sandwichZoneCandidate->getX() + sandwichZoneCandidate->getW() 
					- z.getX();
				
				// Percentage determining how much is the bigger zone enclosing the littler one. The more coeffEnclosure approaches 1, the more one zone is enclosing another.
				double  coeffEnclosure =(double) d / max(sandwichZoneCandidate->getW() , z.getW());
				
				// If the horizontal interval created by the intersection of the 2 parcels zones isn't little enough.
				if( coeffEnclosure > maxHorBorderOverlappingDist)
				{			
					cout<<"\t\tParcelCounting::candidateVertZoneInConfirmedPZone(): sandwich found: "<<*sandwichZoneCandidate<<endl;

					return true;
				}
			}
		} 

	}
		
	
	// Same procedure but this time, we'll look for a sandwich zone located at the right of the origin of z. j2 deduction's is almost the same of j1's as for unique difference to be that this time the search goes towards the right border of z (z.getX() + z.getW()).  
	for(int j = z.getX() ; j < z.getX() + z.getW() ; j++ )
	{ 
		sandwichZoneCandidate = this->getParcelZone(j , z.getY());
		if(sandwichZoneCandidate && sandwichZoneCandidate->getX() <= z.getX() + z.getW() && sandwichZoneCandidate->getX() >= z.getX())
		{
			int d = z.getX() + z.getW() - sandwichZoneCandidate->getX();
			double coeffEnclosure = (double)d / (double) max(sandwichZoneCandidate->getW() , z.getW());
			 
			if(coeffEnclosure > maxHorBorderOverlappingDist)
			{
				cout<<"\tParcelCounting::candidateVertZoneInConfirmedPZone():sandwich found: "<<*sandwichZoneCandidate<<endl;
				return true;
			}
		}	
	}
	return false;

}


/**
 * @brief      Determines if horizontal zone is in confirmed horizontal boundary zone.
 * @details    This procedure allows to avoid creating overlapping horizontal
 *             zones.
 *
 * @param      z     Horizontal zone that will be verified if it's overlapping an existing
 *                   confirmed horizontal boundary zone
 *
 * @return     True if zone in confirmed horizontal boundary zone, False otherwise.
 */

bool ParcelCounting::isHorZoneInConfirmedBoundaryZone(Zone &z)
{
	double verDistParcelSimilarness = this->verDistParcelSimilarnessInt / 1000.0;

	
	// We define i1 as the starting y-axis value from where the search of an overlapping neighbour. (z.getY() - i1) corresponds to the maximal vertical distance (from z.getY()) where the origin of a possible overlapping z could be found.
	int i1= z.getY() - 
		(   (double)
			this->modelLineVerDistParcel.eucledianDist()
			+
			(
				(double)
				this->modelLineVerDistParcel.eucledianDist() 
				* (abs(verDistParcelSimilarness - 1)) - 1
			)
		);
	
	if(i1 < 0 ) i1 = 0;

	


	double maxVerBorderOverlappingDist = this->maxVertBorderOverLappingDistInt / 1000.0;

	// Looking from the i1 y-axis to z.getY(), if there's another parcel zone that could overlap z. If found, we'll refer to it as a sandwich zone.
	shared_ptr<Zone> sandwichZoneCandidate;
	for(; i1< z.getY() ; i1++)
	{
		sandwichZoneCandidate = this->getHorBoundaryZone(z.getX() , i1);
		if(sandwichZoneCandidate)
		{
			cout<<"\tfound vertical sandwichZoneCandidate:"<<*sandwichZoneCandidate<<endl;
			

			if(z.getY() >= sandwichZoneCandidate->getY() && z.getY() <= sandwichZoneCandidate->getY() + sandwichZoneCandidate->getH())
			{

				// The y distance of the zones intersection.
				int d = sandwichZoneCandidate->getY() + sandwichZoneCandidate->getH() 
					- z.getY();
				
				// Percentage determining how much is the bigger zone enclosing the littler one. The more coeffEnclosure approaches 1, the more one zone is enclosing another.
				double  coeffEnclosure =(double) d / max(sandwichZoneCandidate->getH() , z.getH());
				
				// If the vertical interval created by the intersection of the 2 parcels zones isn't little enough.
				if( coeffEnclosure > maxVerBorderOverlappingDist)
				{			
					// sandwichZoneCandidate is a sandiwch enclosing z.
					return true;
				}
			}
		} 

	}
		
	
	// Same procedure but this time, we'll look for a sandwich zone located at the downside of the origin of z. j2 deduction's is almost the same of i1's as for unique difference to be that this time the search goes towards the downside border of z (z.getY() + z.getH()).  
	for(i1 = z.getY() ; i1 < z.getY() + z.getH() ; i1++ )
	{ 
		sandwichZoneCandidate = this->getHorBoundaryZone(z.getX() , i1);
		if(sandwichZoneCandidate && (sandwichZoneCandidate->getY() <= z.getY() + z.getH()) &&(sandwichZoneCandidate->getY() >= z.getY()) )
		{
			int d = z.getY() + z.getH() - sandwichZoneCandidate->getY();

			double coeffEnclosure = (double)d / (double) max(sandwichZoneCandidate->getH() , z.getH());
			 
			if(coeffEnclosure > maxVerBorderOverlappingDist)
			{
				// sandwichZoneCandidate is a sandwich enclosing z;
				return true;
			}
		}	
	}
	return false;

}


/**
 * @brief      Activate horizontal counting mode.
 * @details    Mainly for event control managing. 
 */
void ParcelCounting::activateHorCounting()
{
	this->horCountingInProg = true;	
	this->verCountingInProg = false;
}

/**
 * @brief      Activate vertical counting mode
 * @details    Mainly used for event control managing.
 */
void ParcelCounting::activateVerCounting()
{
	this->verCountingInProg = true;	
	this->horCountingInProg = false;	
}

/**
 * @brief      Checks if horizontal counting mode is activated.
 * @return     True if horizontal counting mode is activated, false otherwise.
 */
bool ParcelCounting::horCountingActive()
{
	return horCountingInProg;
}

/**
 * @brief      Checks if vertical counting mode is activated.
 * @return     True if vertical counting mode is activated, false otherwise.
 */
bool ParcelCounting::verCountingActive()
{
	return verCountingInProg;
}

bool ParcelCounting::isAConfirmedHorizontalZone(Zone &zone)
{
	return this->getHZoneConfirmed(zone.toString()) != nullptr;
	
}

shared_ptr<Zone> ParcelCounting::getHZoneConfirmed(string zoneKey)
{
	map<string,shared_ptr<Zone>>::iterator it;
	it = this->horizontalBordersConfirmed.find(zoneKey);

	if(it != this->horizontalBordersConfirmed.end())
		return it->second;
	else
		return nullptr;
}

/**
 * @brief      Sets the original image.
 *
 * @param[in]  origImageFF  An ImageFF instance
 */
void ParcelCounting::setOrigImage(shared_ptr <ImageFF> origImageFF){
	this->origImageFF = origImageFF;
}