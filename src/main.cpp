#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "Image.hpp"
#include "GaussianFilter.hpp"  
#include "ImageInfoExtractor.hpp"
#include "RegionOfInterestOrientation.hpp"
#include "SelectingRegionsOfInterest.hpp"
#include "State.hpp"
using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    if( argc != 2)
    {
     cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
     return -1;
    } 

    
    shared_ptr<ImageInfoExtractor>imageInfoExtractor= make_shared<ImageInfoExtractor>();
    Mat mat = Mat(imread(argv[1],IMREAD_COLOR));
    if(! mat.data )                              // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    {
        shared_ptr<Image> image= make_shared<Image>(mat);

        imageInfoExtractor->setMainImage(image);
    }
    
  
    
    cout<<"main runing"<<endl;
    

    unique_ptr<SelectingRegionsOfInterest> selectingRegionsOfInterest = make_unique<SelectingRegionsOfInterest>(imageInfoExtractor);
    
    selectingRegionsOfInterest->setColorImage(imageInfoExtractor->getMainImage());
    unsigned int selectingRegionsOfInterestId = State::registerState(move(selectingRegionsOfInterest));
    
    State::execState(selectingRegionsOfInterestId);



    // Infinite loop managing calls for images display.
    while(1)
    {
        waitKey(0);   
    }
    
    
    return 0;
}
