#include "Image.hpp"

Image::Image()
{
	
}

/**
 * @brief      Constructs the object.
 *
 * @param[in]  mat   The matrix representing an image from opencv.
 */
Image::Image(Mat mat)
{
	this->imageMat = mat;
}

Image::~Image()
{
	cout<<"Image::~Image()"<<endl;
}

/**
 * @brief      Gets the matrix mat.
 *
 * @return     The matrix.
 */
Mat Image::getImageMat() 
{
	return this->imageMat;
}


/**
 * @brief      Sets the image matrix.
 *
 * @param[in]  imageMat  The image matrix
 */
void Image::setImageMat(Mat imageMat)
{
	this->imageMat = imageMat;
}


/**
 * @brief      Rotate current mat of angleOfRotation degrees using the center of
 *             the image as the center of rotation.
 *
 * @param[in]  angleOfRotation  The angle of rotation in degrees.
 */
void Image::rotate(double angleOfRotation)
{
  this->rotate(angleOfRotation , this->imageMat);	
}


void Image::rotate(double angleOfRotation , Mat &imageMat)
{
	Mat dstRot;
 
 	//Calculation of the current image center that will be used as the center of rotation
	Point2f center(imageMat.cols/2.0, imageMat.rows/2.0); 
	
	//Calculation of the matrix of rotation. 
	Mat rot = getRotationMatrix2D(center, angleOfRotation, 1.0);
	RotatedRect rRect = RotatedRect(Point2f(),imageMat.size(), angleOfRotation);

	Rect bbox =rRect.boundingRect2f();

	rot.at<double>(0,2) += bbox.width/2.0 - center.x;
	rot.at<double>(1,2) += bbox.height/2.0 - center.y;


	warpAffine(imageMat, dstRot, rot, bbox.size());
	imageMat = dstRot;
	

	this->angleOfOrientation += angleOfRotation; 

	this->lookForBorderPoints();
}

/**
 * @brief      Gets current angle of orientation.
 *
 * @return     The angle of orientation.
 */
double Image::getAngleOfOrientation()
{
	return this->angleOfOrientation;
}


/**
 * @brief      Draws a point of interest represented by a cross in the current
 *             image matrix.
 *
 * @param      poi    The point of interest.
 * @param      color  The color of the cross that will be draw in the matrix
 *                    image.
 */
void Image::drawPointOfInterest(Point &poi , Scalar &color)
{
	
	Mat colorImageMat = this->getImageMat();

	int sizeOfArrowHor = 1.0/50.0 * (double) colorImageMat.cols,
	    sizeOfArrowVer = 1.0/50.0 * (double) colorImageMat.rows;

	int xleft = poi.x - sizeOfArrowHor;
	int xright = poi.x + sizeOfArrowHor;

	int yup = poi.y - sizeOfArrowVer;
	int ydown = poi.y + sizeOfArrowVer;


	if(xleft < 0)
		xleft = 0;
	if(xright >= colorImageMat.cols)
		xright = colorImageMat.cols -1;
	
	if(yup < 0)
		yup = 0;
	if(ydown >= colorImageMat.rows)
		ydown = colorImageMat.rows -1;

	Point pLeft(xleft , poi.y) , pRight(xright , poi.y) , pUp(poi.x , yup) , pDown(poi.x , ydown);

	arrowedLine(colorImageMat, pLeft , poi , color , 10);
	arrowedLine(colorImageMat, pUp   , poi , color , 10);
	arrowedLine(colorImageMat, pRight, poi , color , 10);
	arrowedLine(colorImageMat, pDown , poi , color , 10);
}


void Image::drawPoint(Point &poi , const Vec3b & color)
{
	Mat colorImageMat = this->getImageMat();
	
	if(poi.x < colorImageMat.cols && poi.y < colorImageMat.rows)
	{
		colorImageMat.at<Vec3b>(poi) = color;	
	}

}


void Image::addROIBorderPoint(Point borderPoint)
{
	this->roiBorderPoints.push_back(borderPoint);
}


void Image::lookForBorderPoints()
{
	Mat imageMat = this->getImageMat();
	Vec3b * p;

	Vec3b red(0 , 0 , 255);

	// Border poitns counter.
	int bCpt = 0;

	this->roiBorderPoints.clear();

	for(int i =0 ; i < imageMat.rows ; i ++)
	{
		p = imageMat.ptr<Vec3b> (i);		
		for(int j = 0 ; j < imageMat.cols ; j++)
		{

			if (p[j][2] >= 30 && p[j][1] == 0 && p[j][0] == 0)
			{
				this->addROIBorderPoint(Point(j , i)); 

				if(bCpt == 4)
				{
					Point temp = roiBorderPoints.at(bCpt);
					roiBorderPoints.at(bCpt) = roiBorderPoints.at(bCpt);
					roiBorderPoints.at(bCpt) = temp;
					return;
				}
				bCpt++;

			}	

		}
	}



	cout<<"CannyFilter::loofForBorderPoints() : border points not all found"<<endl<<"\t found :"<< bCpt <<" borders"<<endl;
		

		
}


void Image::drawLine( const Point &p1 , const Point &p2 , const Scalar &color)
{
	line(this->getImageMat() , p1 , p2 , color);
}


void Image::drawGrid()
{
	Mat imageMat = this->getImageMat();

	int horSpacing = imageMat.rows / 100,
		verSpacing = imageMat.cols / 100;

	this->drawHorGrid(horSpacing , Scalar(255 , 255 , 255));
	this->drawVerGrid(verSpacing , Scalar(255 , 255 , 255));	
}

void Image::drawHorGrid(const int horSpacing , const Scalar color )
{
	Mat imageMat = this->getImageMat();
	Point p1,p2;
	p1.x = 0;
	p2.x = imageMat.cols - 1;
	for(int i = 0 ; i < imageMat.rows ; i+= horSpacing)
	{
		p1.y = p2.y = i;
		line(imageMat , p1 , p2 , color , 2 );
	}

}

void Image::drawVerGrid(const int verSpacing , const Scalar color)
{
	Mat imageMat = this->getImageMat();
	Point p1,p2;
	p1.y = 0;
	p2.y = imageMat.rows - 1;
	
	for(int j = 0 ; j < imageMat.rows ; j += verSpacing)
	{
		p1.x = p2.x = j;
		line(imageMat , p1 , p2 , color , 2 );
	}

}
