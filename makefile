
CXX=g++
CXXFLAGS=-std=c++14  -Wall $(shell pkg-config --cflags opencv) 
LDFLAGS += $(shell pkg-config --libs --static opencv) -L /usr/local/lib /usr/local/lib/*.so /usr/local/lib/*.so.*
#LDFLAGS=-L
#IDFLAGS=-I
SCRDIR=src
OUTDIR=build
HEADERDIR=header
HPPs = $(wildcard $(HEADERDIR)/*.hpp)
#$(info HPPs is $(HPPs))
LDLIBS=`pkg-config --libs opencv` -ltbb
INCLUDES = -I$(HEADERDIR)
CMD=$(CXX) $(CXXFLAGS) $^ -o $@  $(LDLIBS) 

$(shell mkdir -p $(OUTDIR))

DR_IMGS_ANALYSIS_OBJS=$(OUTDIR)/main.o $(OUTDIR)/Filter.o $(OUTDIR)/Image.o $(OUTDIR)/ImageInfoExtractor.o $(OUTDIR)/State.o $(OUTDIR)/SelectedRegionOfInterestFiltering.o $(OUTDIR)/CannyFilter.o   $(OUTDIR)/RegionOfInterestOrientation.o $(OUTDIR)/ParcelCounting.o $(OUTDIR)/Zone.o $(OUTDIR)/DrawableGeometStruct.o $(OUTDIR)/Line.o $(OUTDIR)/SelectingRegionsOfInterest.o $(OUTDIR)/ImageFF.o $(OUTDIR)/FilteringForOrientation.o $(OUTDIR)/DummyTrackbar.o

MAIN_ROTATION_TESTS= $(OUTDIR)/mainRotationsTest.o $(OUTDIR)/Image.o $(OUTDIR)/ImageFF.o $(OUTDIR)/CannyFilter.o $(OUTDIR)/Filter.o

MAIN_ROTATION_TESTS1= $(OUTDIR)/mainRotationsTest1.o $(OUTDIR)/Image.o

DEP = $(DR_IMGS_ANALYSIS_OBJS:%.o=%.d)

EROSION_DILATATION_OBJS=$(OUTDIR)/erosion_dilatation_prog.o
	
CANNY_OBJS = $(OUTDIR)/Canny.o


CANNY_DERICHE=$(OUTDIR)/CannyDeriche.o

CANNY_DERICHE2=$(OUTDIR)/CannyDeriche2.o

CANNY_DERICHE3=$(OUTDIR)/CannyDeriche3.o

CONTOURS_DETECTION=$(OUTDIR)/contoursDetection.o

SHAPE_DETECTION = $(OUTDIR)/shapeDetection.o

SPLIT_MERGE =$(OUTDIR)/split_merge.o

TRESHOLDING = $(OUTDIR)/Thresholding.o

COLORS_CANNY = $(OUTDIR)/colors_canny.o

HOUGH = $(OUTDIR)/hough.o

ALIGN =$(OUTDIR)/image_alignment.o

CAMSHIFT = $(OUTDIR)/camshift.o

all : dr_img_analysis mainRotationsTest mainRotationsTest1 #canny colors_canny contours_detection hough align camshift#erosion_dilatation  canny_deriche  thresholding canny_deriche2 canny_deriche3 split_merge  shape_detection
dr_img_analysis: $(DR_IMGS_ANALYSIS_OBJS)
	$(CMD)

# -include $(HPPs)

-include $(DEP)

$(OUTDIR)/%.o: $(SCRDIR)/%.cpp #$(HEADERDIR)/%.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -MMD -c $< -o $@

mainRotationsTest: $(MAIN_ROTATION_TESTS)
	$(CMD)

mainRotationsTest1: $(MAIN_ROTATION_TESTS1)
	$(CMD)

erosion_dilatation: $(EROSION_DILATATION_OBJS)
	$(CMD)

canny : $(CANNY_OBJS)
	$(CMD)

canny_deriche: $(CANNY_DERICHE)
	$(CMD)

canny_deriche2: $(CANNY_DERICHE2)
	$(CMD)

canny_deriche3: $(CANNY_DERICHE3)
	$(CMD)

contours_detection: $(CONTOURS_DETECTION)
	$(CMD)

thresholding : $(TRESHOLDING)
	$(CMD)

split_merge : $(SPLIT_MERGE)
	$(CMD)

shape_detection : $(SHAPE_DETECTION)
	$(CMD)

colors_canny : $(COLORS_CANNY)
	$(CMD)

hough : $(HOUGH)
	$(CMD)

align : $(ALIGN)
	$(CMD)

camshift : $(CAMSHIFT)
	$(CMD)

clean:
	rm -f *.o
	rm -rf $(OUTDIR)

